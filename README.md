# Gloomi

## Summary
Gloomi is a game that was created for the [Ubisoft Game Lab Competition](https://montreal.ubisoft.com/en/our-engagements/education/game-lab-competition/) 2018.

It is the story of two friends, Gloomi & Lumi, who are trying to find light within a dark world.

### Core features
- 2 player cooperative game
- Asymetric (both players do not have same abilities)
- Puzzle solving where cooperation is needed
- Combat against the enemy

![](Preview_Gloomi.gif)

Click [here](https://www.youtube.com/watch?v=U3RnhPBfVLs) for a complete preview.

### Technical Specs
- Platform: PC/Windows 10 (with XBox Controller)
- Game Engine: Unreal 4.17.2

### Awards

In the context of the competition, the game won :
- Audiance Award
- Best Art Direction & Production Award

For more information about prizes [click here](https://montreal.ubisoft.com/en/ubisoft-game-lab-competition-2018-winners/).


### Additional notes
- This game is a LAN (Local Area Network) game.
- No plans have been made to release this game officially as this was done mostly as a side project in the context of the Ubisoft GameLab Competition.


## Development

Prerequisites:
- Unreal 4.17.2
- Git LFS

> :warning: **This project is not actively maintained**: We haven't been maintaining this project since April 2018. This is a project that we are proud of, but was done a while ago and could use some refactoring :smile:. This project was done in 10 weeks, so sometimes shortcuts were taken to prioritize functionality and end result for the players.

## Usage
You may find our last build under the `./Builds` directory.

To play :

On a windows 10 machine:

- Download `Build/finalGloomiBuild.zip`
- Unzip the file
- Follow the steps indicated in file `Steps To Run Game in LAN.txt`
- Have fun ! :stuck_out_tongue_winking_eye:

