// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "DarklingAIController.generated.h"

class UDarklingBehaviour;
class ABaseProjectCharacter;
class UHiveMind;
class ATorch;

/**
 * 
 */
UCLASS()
class UBISOFT2018_API ADarklingAIController : public AAIController
{
	GENERATED_BODY()
	
public:

	void Possess(APawn* PossessedPawn) override;

	float GetDetectionRange();

	UHiveMind * GetHiveMind();

	void AttackTarget(AActor * target);

	UFUNCTION()
	void OnPawnDied(ACharacter* darkling, bool killed);

	UFUNCTION()
	void OnPawnDestroyed(AActor* actor);

	UFUNCTION()
	void OnTargetLost(ACharacter* victim, bool killed);

	UFUNCTION()
	void DecideNextBehaviour();

	class ADarklingCharacter* GetDarkling();

	void SetCurrentBehaviour(UDarklingBehaviour* behaviour);

protected:
	
	void Tick(float DeltaTime) override;

	void OnMoveCompleted(FAIRequestID RequestId, const FPathFollowingResult & result) override;
	
	UFUNCTION()
	void OnStunned(float DeltaTime);

	UFUNCTION()
	void EndStun();

	ATorch* GetNearestProvokingTorch();

	ABaseProjectCharacter* GetClosestTarget();

	UPROPERTY(EditAnywhere)
	float EnemyDetectionRange = 800.0f;

	UPROPERTY(EditAnywhere)
	float ReachedTargetRadius = 100.0f;

	UPROPERTY(EditAnywhere)
	float TimeBetweenDecisions = 1.4f;

	bool bIsMovingTowardTarget = false;

	FTimerHandle StunTimerHandle;

	UPROPERTY()
	bool bIsStunned = false;

	UPROPERTY()
	UHiveMind * HiveMind;

	UPROPERTY()
	UDarklingBehaviour* CurrentBehaviour;
};
