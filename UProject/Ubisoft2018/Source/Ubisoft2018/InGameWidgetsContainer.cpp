// Fill out your copyright notice in the Description page of Project Settings.

#include "InGameWidgetsContainer.h"

void UInGameWidgetsContainer::SetParentController(APlayerController* controller) {
	PlayerController = controller;
}

UUserWidget * UInGameWidgetsContainer::OpenWidget(EWidgetEnum widgetType)
{
	if (OpenWidgets.Contains(widgetType)) {
		return OpenWidgets[widgetType];
	}
	else if (WidgetsClasses.Contains(widgetType)) {
		auto widget = OpenWidgets.Add(widgetType, CreateWidget<UUserWidget>(PlayerController, WidgetsClasses[widgetType]));
		widget->AddToViewport();
		return widget;
	}
	else {
		return nullptr;
	}
}

void UInGameWidgetsContainer::CloseWidget(EWidgetEnum widgetType)
{
	if (OpenWidgets.Contains(widgetType)) {
		OpenWidgets[widgetType]->RemoveFromViewport();
		OpenWidgets.Remove(widgetType);
	}
}

bool UInGameWidgetsContainer::IsWidgetOpen(EWidgetEnum widgetType) {
	return OpenWidgets.Contains(widgetType);
}

