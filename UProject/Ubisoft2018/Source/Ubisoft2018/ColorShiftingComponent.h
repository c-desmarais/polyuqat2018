// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ColorShiftingComponent.generated.h"

class UStaticMeshComponent;

USTRUCT()
struct FMeshMaterialIndexPair
{
	GENERATED_BODY()
public:

	UPROPERTY()
		UStaticMeshComponent* Mesh;
	UPROPERTY()
		int MaterialIndex = 0;
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UBISOFT2018_API UColorShiftingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UColorShiftingComponent();

	void AddTargetMeshMaterial(UStaticMeshComponent* Mesh, int MaterialIndex);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSetIsInLightMode(bool Value);

	void SetIsInLightMode(bool Value);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;


	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	

	UFUNCTION(NetMulticast, Reliable)
	void MulticastSetIsInLightMode(bool Value);

	void MakeAllTargetMaterialsDynamic();

	void SetLightAmount(float Value);

	bool GetIsInLightMode();

	void SetupDynamicMaterial(UStaticMeshComponent* targetMesh, int targetMaterialIndex);

	UPROPERTY()
	TArray<FMeshMaterialIndexPair> TargetMaterials;

	UPROPERTY()
	TArray<class UMaterialInstanceDynamic*> DynamicMaterials;

	float LightAmount;
	
	bool bIsInLightMode = false;

	bool bIsInterpolating = false;

	bool bCreatedDynamicMaterials = false;
};
