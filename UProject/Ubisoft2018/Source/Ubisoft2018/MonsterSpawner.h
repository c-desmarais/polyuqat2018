// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Activator.h"
#include "MonsterSpawner.generated.h"

class AWorldChangerVolume;
class ABaseProjectCharacter;
class USoundCue;
class ASoundManager;

UCLASS()
class UBISOFT2018_API AMonsterSpawner : public AActivator
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMonsterSpawner();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser);

	UFUNCTION(BlueprintCallable)
		float GetHealth();

	UFUNCTION(BlueprintCallable)
		int GetHealthChunks();

	UFUNCTION(BlueprintCallable)
		bool GetIsInvulnerable();

protected:

	void AlertNearbyDarklings();

	ABaseProjectCharacter* GetActivatingPlayer();

	UFUNCTION()
		void DisableInvulnerability();

	UFUNCTION()
		void EnableInvulnerability();

	UFUNCTION()
		void StartVulnerabilityCountdown();
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Subscribes to the activation of invuln disablers
	void ListenToDisablers();

	void ComputeVulnerabilityTick(float deltaTime);
	
	UFUNCTION()
	void PerformSpawnBurst();

	// Called when a monster that belonged to this spawner dies
	UFUNCTION()
	void OnMonsterDeath(ACharacter* Victim, bool bKilled);

	UFUNCTION()
	AActor* GetNextSpawnPoint();
	
	UFUNCTION()
	class ADarklingCharacter* SpawnMonster();

	UFUNCTION(NetMulticast, Reliable)
		void MulticastDamageEffect(bool isInvulnerable);

	void HandleDestruction();

	UFUNCTION(NetMulticast, Reliable)
	void MulticastDestructionEffect();

	void HandleChunkLoss();

	UFUNCTION(NetMulticast, Reliable)
		void MulticastChunkLossEffect(float newMaxHealth);

	void SetLightAngle(float Angle);

	UFUNCTION()
	void OnDestroyedMonsterSpawner(AActor * Act);

	UFUNCTION()
	void PlayShieldSoundEffects(bool isEnabled);

	void DecrementNbMonsterSpawnerNeighbours(); // for sound (when 2 monster spawners)

	UFUNCTION(NetMulticast, Reliable)
	void MulticastLightTorches();

	// The light that shines on the spawner when its vulnerable
	UPROPERTY(EditAnywhere)
	class USpotLightComponent * SpotLightComponent;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* Mesh;

	UPROPERTY()
		UMaterialInstanceDynamic* CrystalMat;

	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|Vulnerability")
	float MaxHealth = 12.0f;

	UPROPERTY(EditAnywhere, Replicated, Category = "MonsterSpawner|Vulnerability")
	int HealthChunks = 3;

	UPROPERTY(EditAnywhere, Replicated, Category = "MonsterSpawner|Vulnerability")
	float HealthRegenRate = 1.0;

	// The activators that will disable invulnerability
	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|Vulnerability")
	TArray<class AActivator*> InvulnerabilityDisablers;

	// How long the spawner is vulnerable when deactivated
	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|Vulnerability")
	float VulnerabilityDuration = 5.0f;

	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|Vulnerability")
	float MaxLightAngle = 30.0f;

	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|Vulnerability")
		float MinLightAngle = 15.0f;

	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|Spawning")
	TArray<AActor*> MonsterSpawnPoints;

	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|Spawning")
	float TimeBetweenSpawnBursts = 3.0f;

	// How wide the god light starts out when vulnerable
	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|Spawning")
	int MonstersSpawnedPerBurst = 1;

	// How many monsters can exist from this monster at any given time
	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|Spawning")
	int MaxMonstersAtATime = 2;

	// The monster to spawn
	UPROPERTY(EditAnywhere)
	TSubclassOf<ADarklingCharacter> MonsterClass;

	bool bIsInvulnerable = true;

	bool bCountingDownVulnerability = false;

	int MonsterCount = 0;

	int MonsterSpawnpointIndex = 0;

	UPROPERTY()
	FTimerHandle SpawnTimerHandle;

	UPROPERTY()
	float VulnerabilityTimeLeft = 0.0f;

	UPROPERTY(Replicated)
	float Health = 0.0f;

	UPROPERTY(Replicated)
	bool bIsHealing = false;

	UPROPERTY()
	float CurrentMaxHealth = 0.0f;

	UPROPERTY()
	int HealthChunksLeft = 0;

	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|Vulnerability")
	float AlertRadius = 2000.0f;

	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|FX")
	USceneComponent* ShieldEffectsRoot;

	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|FX")
		USceneComponent* HealEffectsRoot;

	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|FX")
	UParticleSystem* DestroyParticleEffects;

	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|FX")
		UParticleSystem* DamageParticleEffects;

	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|SFX")
		USceneComponent* SoundEffects;

	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|SFX")
		UAudioComponent* ShieldDownAudioComponent;

	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|SFX")
		UAudioComponent* ShieldUpAudioComponent;

	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|SFX")
		UAudioComponent* HitWhileImmuneAudioComponent;

	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|SFX")
		UAudioComponent* HitWhileVulnerableAudioComponent;
	
	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|SFX")
		USoundCue* DestroySound;

	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|SFX")
		USoundBase* SoundMusicAfterDestroy;

	UPROPERTY(EditAnywhere, Category = "MonsterSpawner|SFX")
		float MusicFadeTimeAfterDestroy = 3.0f;

	UPROPERTY(EditAnywhere)
		TArray<class ATorch*> Torches;

	UPROPERTY(EditAnywhere)
		TArray<AMonsterSpawner*> MonsterSpawnerNeighbours;

	int NbMonsterSpawnerNeighbours = 0;

	ASoundManager* SoundManager;

	UPROPERTY(EditAnywhere)
	UForceFeedbackEffect* DestructionForceFeedback;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float ShowInHUDRange = 2000.f;
};
