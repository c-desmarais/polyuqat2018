// Fill out your copyright notice in the Description page of Project Settings.

#include "BlockAnchor.h"
#include "Components/SphereComponent.h"
#include "Engine/World.h"
#include "Block.h"
#include "Components/StaticMeshComponent.h"


// Sets default values
ABlockAnchor::ABlockAnchor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PreviewMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PreviewMesh"));
	RootComponent = PreviewMesh;

	PreviewMesh->bHiddenInGame = true;
	PreviewMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

// Called when the game starts or when spawned
void ABlockAnchor::BeginPlay()
{
	Super::BeginPlay();

	if (bSpawnBlock && HasAuthority()) {
		SpawnBlock();
	}
}

void ABlockAnchor::SpawnBlock()
{
	ABlock* block = GetWorld()->SpawnActor<ABlock>(BlockClass, GetActorTransform());
	if (block) {
		block->SetCurrentBlockAnchor(this);
	}
}

// Called every frame
void ABlockAnchor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

TArray<ABlockAnchor*> ABlockAnchor::GetNeighbours() {
	return BlockAnchorNeighbours;
}

void ABlockAnchor::SetNeighbours(TArray<ABlockAnchor*> neighbours) {
	BlockAnchorNeighbours = neighbours;
}

