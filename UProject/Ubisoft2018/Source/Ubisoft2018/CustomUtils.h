// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "EngineUtils.h"
#include "CustomUtils.generated.h"

/**
 * 
 */
UCLASS()
class UBISOFT2018_API UCustomUtils : public UObject
{
	GENERATED_BODY()

		public:

		inline static AActor* GetNearestActor(const FVector & Location, const TArray<AActor*> & Actors);
	
		template<class T> inline static  T* GetNearestActorOfType(const FVector & Location, UWorld * World);

	
	
};

template<class T>
inline T * UCustomUtils::GetNearestActorOfType(const FVector & Location, UWorld * World)
{
	T * nearest = nullptr;
	float closestDistance = -1.0;
	for (TActorIterator<T> itr(World); itr; ++itr) {
		float distance = FVector::Dist(Location, itr->GetActorLocation());
		if (distance <= closestDistance || closestDistance == -1.0) {
			closestDistance = distance;
			nearest = *itr;
		}
	}
	return nearest;
}
