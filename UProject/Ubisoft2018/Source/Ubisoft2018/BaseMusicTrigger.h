// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseMusicTrigger.generated.h"

class UBoxComponent;
class ASoundManager;

UCLASS()
class UBISOFT2018_API ABaseMusicTrigger : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseMusicTrigger();
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	ASoundManager* SoundManager;

	UPROPERTY(EditAnywhere)
		UBoxComponent* RootBox;

	UPROPERTY(EditAnywhere)
		bool FadeEffect = true;
	
	UFUNCTION()
		virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	
};
