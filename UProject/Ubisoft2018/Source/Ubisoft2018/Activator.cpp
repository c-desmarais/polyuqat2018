// Fill out your copyright notice in the Description page of Project Settings.

#include "Activator.h"

void AActivator::Activate() {
	ActivationCount++;
	if (!bIsActive) {
		bIsActive = true;
		if (OnActivate.IsBound()) {
			OnActivate.Broadcast();
		}
	}
}

void AActivator::Deactivate()
{
	ActivationCount--;
	if (bIsActive && ActivationCount == 0) {
		bIsActive = false;
		if (OnDeactivate.IsBound()) {
			OnDeactivate.Broadcast();
		}
	}
}

bool AActivator::IsActive()
{
	return bIsActive;
}
