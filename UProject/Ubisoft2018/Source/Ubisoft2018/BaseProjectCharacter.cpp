// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseProjectCharacter.h"
#include "Engine.h"
#include "UnrealNetwork.h"
#include "Ubisoft2018GameModeBase.h"
#include <math.h>
#include "LightCharacter.h"
#include "VisibilityCounterComponent.h"
#include "Components/MeshComponent.h"
#include "WorldChangerVolume.h"


// Sets default values
ABaseProjectCharacter::ABaseProjectCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	bUseControllerRotationYaw = false;

	TargetEffectRoot = CreateDefaultSubobject<UVisibilityCounterComponent>(TEXT("TargetFXRoot"));
	TargetEffectRoot->SetupAttachment(RootComponent);

	SoundEffects = CreateDefaultSubobject<USceneComponent>(TEXT("SoundEffects"));
	SoundEffects->SetupAttachment(RootComponent);

	TakeDamageAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("TakeDamageAudioComponent"));
	TakeDamageAudioComponent->SetupAttachment(SoundEffects);
}

float ABaseProjectCharacter::GetHealth()
{
	return Health;
}

// Called when the game starts or when spawned
void ABaseProjectCharacter::BeginPlay()
{
	// For the Character to rotate (and replicate)
	bUseControllerRotationYaw = false;
	GetCharacterMovement()->bOrientRotationToMovement = true;

	Super::BeginPlay();

	MovementSpeed = GetCharacterMovement()->MaxWalkSpeed;
	CreateDynamicMeshInstances();
	// Get the anim instance
	SetupAnimInstance();

	DissolveAmount = 6;
	bIsSpawning = true;

}

void ABaseProjectCharacter::Restart() {
	Super::Restart();

	APlayerController* PC = Cast<APlayerController>(GetController());
	if (PC) {
		PC->SetAudioListenerOverride(GetRootComponent(), FVector::ZeroVector, GetActorForwardVector().Rotation());
	}
}

// Called every frame
void ABaseProjectCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	UpdateAnimationInstanceValues();

	SetIsMoving(GetCharacterMovement()->GetCurrentAcceleration().Size() > 0);

	if (bIsSpawning) {
		DissolveAmount -= DeltaTime* DissolveRateMultiplier;
		DissolveAmount = FMath::Clamp(DissolveAmount, -1.0f, 6.0f);

		for (UMaterialInstanceDynamic* material : DynamicMaterials) {
			material->SetScalarParameterValue(FName("Disolve"), DissolveAmount);
		}

		if (DissolveAmount == -1.0f) {
			bIsSpawning = false;
		}
	}

}

bool ABaseProjectCharacter::CanAttack()
{
	return !CharacterAnimParameters.bIsDying && !CharacterAnimParameters.bIsFalling && !CharacterAnimParameters.bIsAttacking && !CharacterAnimParameters.bIsTakingDamage;
}

void ABaseProjectCharacter::ServerEnqueueAttackRequest_Implementation(FVector AttackDirection)
{
	if (CanAttack()) {
		AttackDirection = AdjustFacingVector(AttackDirection);
		CharacterAnimParameters.bIsAttacking = true;
		ReplicateAnimParameters(AttackDirection);
	}
}

bool ABaseProjectCharacter::ServerEnqueueAttackRequest_Validate(FVector AttackDirection)
{
	return true;
}

void ABaseProjectCharacter::SetupAnimInstance()
{
	GameAnimInstance = Cast<UUbisoftAnimInstance>(GetMesh()->GetAnimInstance());
	
	GameAnimInstance->OnResolveAttack.AddDynamic(this, &ABaseProjectCharacter::OnResolveAttack);
	GameAnimInstance->OnAttackCompleted.AddDynamic(this, &ABaseProjectCharacter::OnFinishedAttackAnimation);
	GameAnimInstance->OnHurtCompleted.AddDynamic(this, &ABaseProjectCharacter::OnFinishedHurtAnimation);
	GameAnimInstance->OnDeathAnimCompleted.AddDynamic(this, &ABaseProjectCharacter::OnFinishedDeathAnimation);
}

void ABaseProjectCharacter::UpdateAnimationInstanceValues()
{
	if (GameAnimInstance) {
		GameAnimInstance->SetMovementSpeed(GetVelocity().Size());
		CharacterAnimParameters.bIsFalling = GetMovementComponent()->IsFalling();
		GameAnimInstance->SetAnimationParameters(CharacterAnimParameters);
	}
}

void ABaseProjectCharacter::ReplicateAnimParameters()
{
	MulticastUpdateAnimParameters(GetActorForwardVector(), CharacterAnimParameters);
}

void ABaseProjectCharacter::ReplicateAnimParameters(FVector ActorFacing)
{
	MulticastUpdateAnimParameters(ActorFacing, CharacterAnimParameters);
}

void ABaseProjectCharacter::Move(FVector MovementDirection, float DeltaTime)
{
	if (!CharacterAnimParameters.bIsAttacking) {
		AddMovementInput(MovementDirection, MovementSpeed);
	}
}

void ABaseProjectCharacter::Attack(FVector AttackDirection)
{
	ServerEnqueueAttackRequest(AttackDirection);
}

FVector ABaseProjectCharacter::AdjustFacingVector(FVector AttackDirection)
{
	if (AttackDirection.Size() < 0.1f) {
		AttackDirection = GetActorForwardVector();
	}
	AttackDirection.Z = 0.0f;
	return AttackDirection;
}


void ABaseProjectCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABaseProjectCharacter, Health);
}

float ABaseProjectCharacter::TakeDamage(float DamageAmount, FDamageEvent const & DamageEvent, AController * EventInstigator, AActor * DamageCauser)
{
	const float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (ActualDamage > 0.f)
	{
		Health -= ActualDamage;
		if (Health <= 0.f)
		{
			Die();
		}
		else {

			CharacterAnimParameters.bIsAggro = false;
			ServerActivateHurtEffects(1.0f);

			CharacterAnimParameters.bIsTakingDamage = true;
			ReplicateAnimParameters();
			// Interupt Attack
			OnFinishedAttackAnimation();
		}
	}

	return ActualDamage;
}

void ABaseProjectCharacter::Die()
{
	if (HasAuthority())
	{
		ServerPlayDeathParticle();
		ServerPlayDeathSound();
	}
	if (OnDeath.IsBound()) {
		OnDeath.Broadcast(this, true);
	}
}


void ABaseProjectCharacter::PushAction()
{
	// Implement only for yoota , lumi does nothing
}

void ABaseProjectCharacter::MulticastUpdateAnimParameters_Implementation(FVector ActorFacing, FAnimParameters parameters)
{
	FRotator actorRotation = FRotator(0, ActorFacing.Rotation().Yaw, 0);
	SetActorRotation(actorRotation);
	CharacterAnimParameters = parameters;
}

void ABaseProjectCharacter::OnResolveAttack()
{
	if (HasAuthority()) {
		for (TActorIterator<ABaseProjectCharacter> iterator(GetWorld()); iterator; ++iterator) {
			if (IsValidTarget(*iterator)) {
				FDamageEvent damageEvent(UDamageType::StaticClass());
				iterator->TakeDamage(AttackDamage, damageEvent, GetController(), this);
			}
		}
	}
}

bool ABaseProjectCharacter::IsValidTarget(AActor * actor)
{
	FVector vectorToTarget = actor->GetActorLocation() - GetActorLocation();
	vectorToTarget.Z = 0;
	bool targetInAttackAngle = FVector::Coincident(vectorToTarget, GetActorForwardVector(), FMath::Cos(AttackAngle));
	bool targetInAttackRange = vectorToTarget.Size() < MeleeRange;
	return targetInAttackAngle && targetInAttackRange;
}

void ABaseProjectCharacter::OnStartMoving()
{
}

void ABaseProjectCharacter::OnStopMoving()
{
}

void ABaseProjectCharacter::OnFinishedAttackAnimation()
{
	if (HasAuthority()) {
		CharacterAnimParameters.bIsAttacking = false;
		ReplicateAnimParameters();
	}
}

void ABaseProjectCharacter::OnFinishedDeathAnimation()
{
	if (OnDeath.IsBound()) {
		OnDeath.Broadcast(this, true);
	}
}

void ABaseProjectCharacter::OnFinishedHurtAnimation()
{
	if (HasAuthority()) {
		CharacterAnimParameters.bIsTakingDamage = false;
		ReplicateAnimParameters();

		ServerActivateHurtEffects(0.0f);
	}
}

void ABaseProjectCharacter::ServerDispatchLightAction_Implementation()
{
	//Implement on lumi side only
}

bool ABaseProjectCharacter::ServerDispatchLightAction_Validate()
{
	return true;
}

void ABaseProjectCharacter::AddSpeedModifier(float ratio)
{
	SpeedModifiers.Add(ratio);
	SpeedModifiers.Sort();
	UpdateToCurrentSpeed();


}

void ABaseProjectCharacter::RemoveSpeedModifier(float ratio)
{
	SpeedModifiers.RemoveSingle(ratio);
	UpdateToCurrentSpeed();

	
}

void ABaseProjectCharacter::UpdateToCurrentSpeed()
{
	float baseSpeed = MovementSpeed;

	for (float modifier : SpeedModifiers) {
		baseSpeed = baseSpeed * modifier;
	}
	GetCharacterMovement()->MaxWalkSpeed = baseSpeed;
}

void ABaseProjectCharacter::CreateDynamicMeshInstances()
{
	auto mesh = GetMesh();
	if (mesh) {
		auto NbMaterials = mesh->GetMaterials().Num();
		for (int i = 0; i < NbMaterials; i++) {
			DynamicMaterials.Add(mesh->CreateDynamicMaterialInstance(i));
		}
	}
}

bool ABaseProjectCharacter::ServerActivateHurtEffects_Validate(float Hurt)
{
	return true;
}

void ABaseProjectCharacter::ServerActivateHurtEffects_Implementation(float Hurt)
{
	MulticastChangeMaterial(Hurt);
	if (Hurt > 0) {
		MulticastPlayHurtParticleEffect();
		MulticastPlayTakeDamageSound();
	}
}

void ABaseProjectCharacter::MulticastChangeMaterial_Implementation(float Hurt) 
{
	for (UMaterialInstanceDynamic* material : DynamicMaterials) {
		material->SetScalarParameterValue(FName("Hurt"), Hurt);
	}
}

void ABaseProjectCharacter::MulticastPlayHurtParticleEffect_Implementation()
{
	//Spawn damage particles
	if (TakeDamageParticleEffects) {
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TakeDamageParticleEffects, GetActorLocation());
	}
}

void ABaseProjectCharacter::MulticastPlayTakeDamageSound_Implementation()
{
	if (TakeDamageAudioComponent) {
		TakeDamageAudioComponent->Play();
	}
}

void ABaseProjectCharacter::MulticastPlayDeathSound_Implementation()
{
	//Play sound
	if (DeathSound) {
		UGameplayStatics::PlaySound2D(GetWorld(), DeathSound);
	}
}

bool ABaseProjectCharacter::ServerPlayDeathSound_Validate()
{
	return true;
}

void ABaseProjectCharacter::ServerPlayDeathSound_Implementation()
{
	MulticastPlayDeathSound();
}

void ABaseProjectCharacter::MulticastPlayDeathParticle_Implementation()
{
	if (DeathParticleEffects) {
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DeathParticleEffects, GetActorLocation());
	}
}

bool ABaseProjectCharacter::ServerPlayDeathParticle_Validate()
{
	return true;
}

void ABaseProjectCharacter::ServerPlayDeathParticle_Implementation()
{
	MulticastPlayDeathParticle();
}

void ABaseProjectCharacter::SetIsMoving(bool IsMoving)
{
	if (IsMoving == bIsMoving)
		return;

	if (IsMoving)
	{
		OnStartMoving();
		bIsMoving = true;
	}
	else
	{
		OnStopMoving();
		bIsMoving = false;
	}
}

bool ABaseProjectCharacter::IsInLightModeWorldChanger()
{
	TArray<AActor*> OverlappingActors;
	GetOverlappingActors(OverlappingActors, AWorldChangerVolume::StaticClass());

	for (AActor* Actor : OverlappingActors) {
		if (Actor) {
			auto worldChanger = Cast<AWorldChangerVolume>(Actor);
			if (worldChanger) {
				return worldChanger->IsInLightMode();
			}
		}
	}

	return false;
}