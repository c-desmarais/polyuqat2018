// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Torch.generated.h"

class USphereComponent;
class UDecalComponent;
class UColorShiftingComponent;
class AWorldChangerVolume;
class USoundBase;

UCLASS()
class UBISOFT2018_API ATorch : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATorch();

	void LightTorch();

	void DimTorch();
	
	bool GetIsLit();

	USphereComponent * GetProvocationCollider();

	UFUNCTION()
		void OnOverlapTorchActivationAreaBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnOverlapTorchActivationAreaEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		void OnOverlapSlowAreaBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	TArray<AWorldChangerVolume*> GetWorldChangers();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(NetMulticast, Reliable)
	void MulticastSetTorchLit(bool bLit);

	void PlaySounds(bool bLit);

	// Called when torch is lit to apply debuff to those in radius
	void ProvokeOverlappingDarklings();

	void NotifyWorldChangerOfTorchStatus(bool isLit);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastRemoveTorchLitFromWorldChanger(AWorldChangerVolume* WorldChanger);

	bool bIsLit = false;

	UPROPERTY(EditAnywhere)
	UColorShiftingComponent* ColorShift;

	UPROPERTY(EditDefaultsOnly)
		USceneComponent* Root;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* MeshTorch;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* MeshOrb;

	UPROPERTY(EditAnywhere)
		USphereComponent* TorchActivationArea;

	UPROPERTY(EditAnywhere)
	USphereComponent* DarklingProvocationArea;

	UPROPERTY(EditAnywhere)
	class UVisibilityCounterComponent * TargetEffectRoot;

	UPROPERTY(EditAnywhere)
	TArray<AWorldChangerVolume*> WorldChangers;

	UPROPERTY(EditAnywhere)
		UParticleSystem* TorchParticleSystem = nullptr;

	UPROPERTY(EditAnywhere)
		USceneComponent* LightNearbyEffects;

	UPROPERTY(EditAnywhere)
		UAudioComponent* OnOffAudioComponent;

	UPROPERTY(EditAnywhere)
		UAudioComponent* AmbiantAudioComponent;

	UPROPERTY(EditAnywhere)
		USceneComponent* SoundEffects;

	UPROPERTY(EditAnywhere)
		USoundBase* OnSound;

	UPROPERTY(EditAnywhere)
		USoundBase* OffSound;

	UParticleSystemComponent* CurrentPS = nullptr;

	UPROPERTY(EditAnywhere)
	int MaxHealth = 100;

	int Health;
};
