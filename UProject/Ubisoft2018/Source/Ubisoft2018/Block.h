// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Block.generated.h"

class UBoxComponent;
class ABlockAnchor;
class ADepressedCharacter;
class UColorShiftingComponent;
class USceneComponent;

UCLASS()
class UBISOFT2018_API ABlock : public AActor
{
	GENERATED_BODY()
	
public:	
	ABlock();

	virtual void Tick(float DeltaTime) override;

	ABlockAnchor* CalculateDestinationBlockAnchor(FVector depressedPosition);

	void SetCurrentBlockAnchor(ABlockAnchor* blockAnchor);

	bool IsPositionWithinBlockRadius(FVector position);

	UFUNCTION(Client, Reliable)
		void ClientDisplayCanInterract(bool IsDisplayed, ADepressedCharacter* depressed);

	void MoveBlockToDestination(ABlockAnchor* blockAnchor);

	UFUNCTION(NetMulticast, Reliable)
		void MulticastStartMove();

	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);

	UPROPERTY(EditAnywhere)
		float SpeedMultiplier = 500.0f;
	
protected:
	virtual void BeginPlay() override;

	bool IsActorBlockingTheWayToDestination(AActor* actor, float blockingAngleThreshold);

	void InitializeTrajectory(ABlockAnchor* currentAnchorPoint);

	TArray<AActor*> GetBlockingActors();

	bool PushingTowardsBlockOrWall(FVector depressedPosition);

	ABlockAnchor* FindDestinationBlockAnchor(FVector depressedPosition, FVector blockPosition, TArray<ABlockAnchor*> possibleDestinations);

	ABlockAnchor* IsBlockOnAnchorPoint();

	void StopMovement();

	UFUNCTION(NetMulticast, Reliable)
		void MulticastStopMovement();

	void ProcessMovement(float deltaTime);

	UFUNCTION(NetMulticast, Reliable)
		void MulticastRPCPutBlockOnHold();

	UPROPERTY(EditAnywhere)
		ABlockAnchor* CurrentBlockAnchor = nullptr;

	UPROPERTY(EditAnywhere)
		USceneComponent* Root;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere)
		UBoxComponent* Box;

	UPROPERTY(EditAnywhere)
		UColorShiftingComponent* ColorShift;

	UPROPERTY(EditAnywhere)
		float CharacterBlockingAngleThreshold = 75.0f;

	UPROPERTY(EditAnywhere)
		float WorldBlockingAngleThreshold = 40.0f;

	UPROPERTY(EditAnywhere)
		USceneComponent* DepressedNearbyEffects;

	// The amount you want to add to the value : (width of the block)/2. Used for setting the dist from which the block can be pushed.
	UPROPERTY(EditAnywhere)
		float BlockExtraRadius = 150.0f;

	UPROPERTY(EditAnywhere)
		UAudioComponent* InputAudioComponent;

	UPROPERTY(EditAnywhere)
		UAudioComponent* MovementAudioComponent;

	UPROPERTY(EditAnywhere)
		USceneComponent* SoundEffects;

	UPROPERTY(EditAnywhere)
		USoundBase* PushSound;

	UPROPERTY(EditAnywhere)
		USoundBase* LandSound;

	bool IsMoving = false;
	bool InATrajectory = false;
	FVector SourcePosition = FVector(0, 0, 0);
	ABlockAnchor* DestinationAnchor = nullptr;
	ABlockAnchor* SourceAnchor = nullptr;
	float minDistanceBtwBlockAndUnderAnchorPoint = 20.f;
	float BlockRadius = 0.0f;

	FVector MoveDirection;
	
	
};
