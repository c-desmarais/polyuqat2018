// Fill out your copyright notice in the Description page of Project Settings.

#include "WorldChangerVolumeTrigger.h"
#include "Components/BoxComponent.h"
#include "WorldChangerVolume.h"

// Sets default values
AWorldChangerVolumeTrigger::AWorldChangerVolumeTrigger()
{
	TriggerZone = CreateDefaultSubobject<UBoxComponent>(TEXT("TriggerZone"));
	TriggerZone->SetupAttachment(RootComponent);
	RootComponent = TriggerZone;

	TriggerZone->OnComponentBeginOverlap.AddDynamic(this, &AWorldChangerVolumeTrigger::OnOverlapBegin);

}

void AWorldChangerVolumeTrigger::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (WorldChangeVolume) {
		WorldChangeVolume->ChangeWorld(true);
	}
}
