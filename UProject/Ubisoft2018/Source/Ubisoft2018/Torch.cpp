// Fill out your copyright notice in the Description page of Project Settings.

#include "Torch.h"
#include "Components/SphereComponent.h"
#include "Components/DecalComponent.h"
#include "Engine.h"
#include "BaseProjectCharacter.h"
#include "DarklingCharacter.h"
#include "DarklingAIController.h"
#include "LightCharacter.h"
#include "DepressedCharacter.h"
#include "ColorShiftingComponent.h"
#include "VisibilityCounterComponent.h"
#include "WorldChangerVolume.h"


// Sets default values
ATorch::ATorch()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	TorchActivationArea = CreateDefaultSubobject<USphereComponent>(TEXT("TorchActivationArea"));
	TorchActivationArea->SetSphereRadius(150.f);
	TorchActivationArea->SetupAttachment(RootComponent);

	DarklingProvocationArea = CreateDefaultSubobject<USphereComponent>(TEXT("SlowArea"));
	DarklingProvocationArea->SetSphereRadius(400.f);
	DarklingProvocationArea->SetupAttachment(RootComponent);

	MeshTorch = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshTorch"));
	MeshTorch->SetupAttachment(RootComponent);

	MeshOrb = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshOrb"));
	MeshOrb->SetupAttachment(RootComponent);

	TorchActivationArea->OnComponentBeginOverlap.AddDynamic(this, &ATorch::OnOverlapTorchActivationAreaBegin);

	DarklingProvocationArea->OnComponentBeginOverlap.AddDynamic(this, &ATorch::OnOverlapSlowAreaBegin);

	ColorShift = CreateDefaultSubobject<UColorShiftingComponent>(TEXT("ColorShift"));

	TargetEffectRoot = CreateDefaultSubobject<UVisibilityCounterComponent>(TEXT("TargetFXRoot"));
	TargetEffectRoot->SetupAttachment(RootComponent);

	LightNearbyEffects = CreateDefaultSubobject<USceneComponent>(TEXT("LightNearbyEffects"));
	LightNearbyEffects->SetupAttachment(RootComponent);

	// sound effects
	SoundEffects = CreateDefaultSubobject<USceneComponent>(TEXT("SoundEffects"));
	SoundEffects->SetupAttachment(RootComponent);

	OnOffAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("OnOffAudioComponent"));
	OnOffAudioComponent->SetupAttachment(SoundEffects);

	AmbiantAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AmbiantAudioComponent"));
	AmbiantAudioComponent->SetupAttachment(SoundEffects);
	
}

// Called when the game starts or when spawned
void ATorch::BeginPlay()
{
	Super::BeginPlay();
	MeshOrb->SetVisibility(false);
	MeshOrb->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	
	ColorShift->AddTargetMeshMaterial(MeshTorch, 0);
	
	// Tried putting this in constructor, and it doesnt trigger the event when it is in there, so hence why in BeginPlay. 
	TorchActivationArea->OnComponentEndOverlap.AddDynamic(this, &ATorch::OnOverlapTorchActivationAreaEnd);

	LightNearbyEffects->SetHiddenInGame(true, true);
	Health = MaxHealth;
}



void ATorch::LightTorch()
{
	if (TorchParticleSystem) {
		if (CurrentPS) {
			CurrentPS->SetVisibility(true, false);
			CurrentPS->Activate(true);
		}
		else {
			CurrentPS = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TorchParticleSystem, GetActorLocation());
		}
	}
	Health = MaxHealth;
	if (!bIsLit) {
		//Send message to worldchanger that is lit
		NotifyWorldChangerOfTorchStatus(true);
	}
	MulticastSetTorchLit(true);	
	ProvokeOverlappingDarklings();
}

void ATorch::DimTorch()
{
	if (HasAuthority()) {
		Health--;
	}

	if (Health <= 0) {
		if (bIsLit) {
			//Send message to world changer that is dimmed
			NotifyWorldChangerOfTorchStatus(false);
		}
		MulticastSetTorchLit(false);
	}
}

void ATorch::MulticastSetTorchLit_Implementation(bool bLit)
{
	bIsLit = bLit;
	if (!bLit && TorchParticleSystem) {
		if (CurrentPS)
		{
			//CurrentPS->DestroyComponent();
			CurrentPS->SetVisibility(false, false);
		}
	}
	MeshOrb->SetVisibility(bLit);
	MeshOrb->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	ColorShift->SetIsInLightMode(bLit);
	for (AWorldChangerVolume* WorldChanger : WorldChangers) {
		if (WorldChanger) {
			WorldChanger->ChangeWorld(bLit);
		}
	}
	LightNearbyEffects->SetHiddenInGame(true, true);

	PlaySounds(bLit);
}

void ATorch::ProvokeOverlappingDarklings()
{
	TArray<AActor*> overlappedDarklings;
	DarklingProvocationArea->GetOverlappingActors(overlappedDarklings, ADarklingCharacter::StaticClass());
	for (AActor* darkling : overlappedDarklings) {
		auto darklingCharacter = Cast<ADarklingCharacter>(darkling);
		if (darklingCharacter) {
			ADarklingAIController* darklingController= Cast<ADarklingAIController>(darklingCharacter->GetController());
			if (darklingController) {
				darklingController->DecideNextBehaviour();
			}
		}
	}
}

bool ATorch::GetIsLit()
{
	return bIsLit;
}

USphereComponent * ATorch::GetProvocationCollider()
{
	return DarklingProvocationArea;
}

void ATorch::OnOverlapTorchActivationAreaBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ALightCharacter* lightCharacter = Cast<ALightCharacter>(OtherActor);
	if (!bIsLit && lightCharacter) {
		lightCharacter->SetNearbyTorch(this);
		if (lightCharacter->IsLocallyControlled()) {
			LightNearbyEffects->SetHiddenInGame(false, true);
		}
	}
}

void ATorch::OnOverlapTorchActivationAreaEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ALightCharacter* lightCharacter = Cast<ALightCharacter>(OtherActor);
	if (lightCharacter) {
		lightCharacter->SetNearbyTorch(nullptr);
		if (lightCharacter->IsLocallyControlled()) {
			LightNearbyEffects->SetHiddenInGame(true, true);
		}
	}
	
}

void ATorch::OnOverlapSlowAreaBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (HasAuthority() && bIsLit) {
		// slow nearby darklings
		ADarklingCharacter* darkling = Cast<ADarklingCharacter>(OtherActor);
		if (darkling) {
			Cast<ADarklingAIController>(darkling->GetController())->DecideNextBehaviour();
		}
	}
}

void ATorch::PlaySounds(bool bLit) {
	if (bLit) {
		OnOffAudioComponent->SetSound(OnSound);
		OnOffAudioComponent->Play();
		AmbiantAudioComponent->Play();
	}
	else {
		OnOffAudioComponent->SetSound(OffSound);
		OnOffAudioComponent->Play();
		AmbiantAudioComponent->Stop();
	}
}

TArray<AWorldChangerVolume*> ATorch::GetWorldChangers()
{
	return WorldChangers;
}

void ATorch::NotifyWorldChangerOfTorchStatus(bool isLit)
{
	for (AWorldChangerVolume* WorldChanger : WorldChangers) {
		if (WorldChanger) {
			if (isLit) {
				//Add torch to lit torches
				WorldChanger->AddLitTorch();
			}
			else {
				MulticastRemoveTorchLitFromWorldChanger(WorldChanger);
			}
		}
	}
}

void ATorch::MulticastRemoveTorchLitFromWorldChanger_Implementation(AWorldChangerVolume* WorldChanger)
{
	//Remove torch from lit torches
	WorldChanger->RemoveLitTorch();
}