// Fill out your copyright notice in the Description page of Project Settings.

#include "WaveProjectile.h"
#include "BaseProjectCharacter.h"
#include "DarklingCharacter.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

AWaveProjectile::AWaveProjectile(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Use a sphere as a simple collision representation
	CollisionComponent = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("SphereComp"));
	CollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &AWaveProjectile::OnOverlapBegin);
	RootComponent = CollisionComponent;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = ObjectInitializer.CreateDefaultSubobject<UProjectileMovementComponent>(this, TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComponent;
	ProjectileMovement->InitialSpeed = 1000.f;
	ProjectileMovement->MaxSpeed = 2000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = false;
	ProjectileMovement->Bounciness = 1.f;
	ProjectileMovement->ProjectileGravityScale = 0.f;
	bReplicates = false;
	bReplicateMovement = false;
}

// Called when the game starts or when spawned
void AWaveProjectile::BeginPlay()
{
	Super::BeginPlay();
	bReplicates = false;
	bReplicateMovement = false;
	
}

// Called every frame
void AWaveProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWaveProjectile::Launch(const FVector& ShootDirection)
{
	if (ProjectileMovement)
	{
		// set the projectile's velocity to the desired direction
		ProjectileMovement->Velocity = ShootDirection * ProjectileMovement->InitialSpeed;
	}

	if (SpawnSound) {
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), SpawnSound, GetActorLocation());
	}
}

void AWaveProjectile::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (HasAuthority())
	{
		auto darkling = Cast<ADarklingCharacter>(OtherActor);
		if (darkling) {
			darkling->Stun(StunTime);
			MulticastPlayHitEffects(darkling->GetActorLocation());
		}
	}

}

void AWaveProjectile::MulticastPlayHitEffects_Implementation(FVector Location)
{
	if (HitSound) {
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, Location);
	}

	if (HitParticleEffects) {
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitParticleEffects, Location);
	}
}



