// Fill out your copyright notice in the Description page of Project Settings.

#include "Bridge.h"
#include "Activator.h"
#include "Components/StaticMeshComponent.h"
#include "Materials/MaterialInstanceDynamic.h"


// Sets default values
ABridge::ABridge()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	NbSwitchesActivated = 0;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);

	SoundEffects = CreateDefaultSubobject<USceneComponent>(TEXT("SoundEffects"));
	SoundEffects->SetupAttachment(RootComponent);

	OnOffAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("OnOffAudioComponent"));
	OnOffAudioComponent->SetupAttachment(SoundEffects);

	AmbiantAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AmbiantAudioComponent"));
	AmbiantAudioComponent->SetupAttachment(SoundEffects);

	bIsInterpolating = false;
	MaterialAmount = 1.0f;
}

// Called when the game starts or when spawned
void ABridge::BeginPlay()
{
	Super::BeginPlay();
	SetActorHiddenInGame(true);
	SetActorEnableCollision(false);
	ListenToActivators();

	DynamicMaterial = Mesh->CreateDynamicMaterialInstance(0);
}

// Called every frame
void ABridge::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsInterpolating) {
		MaterialAmount -= bIsFadingIn ? DeltaTime: -DeltaTime;
		MaterialAmount = FMath::Clamp(MaterialAmount, 0.0f, 1.0f);

		DynamicMaterial->SetScalarParameterValue(FName("Amount"), MaterialAmount);

		if (MaterialAmount == 0.0f || MaterialAmount == 1.0f) {
			bIsInterpolating = false;
			if (!bIsFadingIn) {
				HideBridge();
			}
		}
	}

}

void ABridge::ListenToActivators()
{
	for (AActivator* activator : Activators) {
		activator->OnActivate.AddDynamic(this, &ABridge::Activate);
		activator->OnDeactivate.AddDynamic(this, &ABridge::DeActivate);
	}
}

void ABridge::ShowBridge()
{
	bIsFadingIn = true;
	bIsInterpolating = true;
	SetActorHiddenInGame(false);
	SetActorEnableCollision(true);
}

void ABridge::HideBridge()
{
	MaterialAmount = 1.0f;
	SetActorHiddenInGame(true);
	SetActorEnableCollision(false);
}

void ABridge::Activate() {
	NbSwitchesActivated++;
	if (NbSwitchesActivated == 1) {
		OnOffAudioComponent->SetSound(OnActivateSound);
		OnOffAudioComponent->Play();
		AmbiantAudioComponent->Play();
		ShowBridge();
	}	
}

void ABridge::DeActivate() {
	NbSwitchesActivated--;
	if (NbSwitchesActivated == 0) {
		OnOffAudioComponent->SetSound(OnDeactivateSound);
		OnOffAudioComponent->Play();
		AmbiantAudioComponent->Stop();
		bIsInterpolating = true;
		bIsFadingIn = false;
		//HideBridge();
	}
}