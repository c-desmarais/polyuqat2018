// Fill out your copyright notice in the Description page of Project Settings.

#include "UbisoftAnimInstance.h"

void UUbisoftAnimInstance::SetAnimationParameters(const FAnimParameters& newParams)
{
	AnimationParameters = newParams;
}

void UUbisoftAnimInstance::SetMovementSpeed(float Speed)
{
	MovementSpeed = Speed;
}

