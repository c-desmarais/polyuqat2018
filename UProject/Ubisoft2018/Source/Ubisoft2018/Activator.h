// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Activator.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FActivationEvent);

UCLASS()
class UBISOFT2018_API AActivator : public AActor
{
	GENERATED_BODY()
	
public:

	void Activate();

	void Deactivate();

	bool IsActive();

	FActivationEvent OnActivate;

	FActivationEvent OnDeactivate;

private:

	bool bIsActive = false;

	int ActivationCount = 0;

};
