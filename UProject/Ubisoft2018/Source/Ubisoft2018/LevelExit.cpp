// Fill out your copyright notice in the Description page of Project Settings.

#include "LevelExit.h"
#include "Activator.h"
#include "Engine/World.h"
#include "Components/BoxComponent.h"
#include "Ubisoft2018GameModeBase.h"
#include "DepressedCharacter.h"
#include "LightCharacter.h"
#include "WorldChangerVolume.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

// Sets default values
ALevelExit::ALevelExit()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	StaticMeshCabin = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshCabin"));
	StaticMeshCabin->SetupAttachment(RootComponent);

	Doors = CreateDefaultSubobject<USceneComponent>(TEXT("Doors"));
	Doors->SetupAttachment(RootComponent);

	SkeletalMeshLeftDoor = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMeshLeftDoor"));
	SkeletalMeshLeftDoor->SetupAttachment(Doors);

	SkeletalMeshRightDoor = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMeshRightDoor"));
	SkeletalMeshRightDoor->SetupAttachment(Doors);

	LevelEndBox = CreateDefaultSubobject<UBoxComponent>(TEXT("LevelEndBox"));
	LevelEndBox->SetupAttachment(RootComponent);

	LevelEndBox->OnComponentBeginOverlap.AddDynamic(this, &ALevelExit::OnOverlapBegin);
}

// Called when the game starts or when spawned
void ALevelExit::BeginPlay()
{
	Super::BeginPlay();

	ListenToActivators();
}

void ALevelExit::ListenToActivators()
{
	for (AActivator * activator : Activators) {
		activator->OnActivate.AddDynamic(this, &ALevelExit::OnActivated);
		activator->OnDeactivate.AddDynamic(this, &ALevelExit::OnDeactivated);
	}
}

void ALevelExit::OnActivated()
{
	if (HasAuthority()) {
		nbActivations++;
		if (nbActivations >= nbActivationsNecessary) {
			MulticastChangeWorld();
			MulticastOpenDoor();
		}
	}
}

void ALevelExit::OnDeactivated()
{
	if (HasAuthority()) {
		nbActivations--;
	}
}

// Called every frame
void ALevelExit::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALevelExit::MulticastOpenDoor_Implementation() {
	SkeletalMeshRightDoor->Play(false);
	SkeletalMeshLeftDoor->Play(false);
	if (LevelExitForceFeedback)
	{
		UGameplayStatics::SpawnForceFeedbackAtLocation(GetWorld(), LevelExitForceFeedback, GetActorLocation());
	}
}

void ALevelExit::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!HasAuthority()) return; 

	if (OtherActor->IsA(ALightCharacter::StaticClass())) {
		bLightCharacterPassed = true;
		OtherActor->Destroy();
	}
	else if (OtherActor->IsA(ADepressedCharacter::StaticClass())) {
		bDepressedCharacterPassed = true;
		OtherActor->Destroy();
	}

	if (bDepressedCharacterPassed && bLightCharacterPassed) {
		Cast<AUbisoft2018GameModeBase>(GetWorld()->GetAuthGameMode())->EndGame();
	}
}

void ALevelExit::MulticastChangeWorld_Implementation()
{
	//Change world
	for (AWorldChangerVolume* worldChangeVolume : WorldChangers) {
		if (worldChangeVolume) {
			worldChangeVolume->ChangeWorld(true);
		}
	}
}



