// Fill out your copyright notice in the Description page of Project Settings.

#include "DarklingCharacter.h"
#include "DarklingAIController.h"
#include "EngineUtils.h"
#include "LightCharacter.h"
#include "DepressedCharacter.h"
#include "UbisoftAnimInstance.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine.h"
#include "Particles/ParticleSystemComponent.h"

ADarklingCharacter::ADarklingCharacter() {
	StatusBillboard = CreateDefaultSubobject<UMaterialBillboardComponent>(TEXT("StatusBillboard"));
	StatusBillboard->SetupAttachment(RootComponent);

	TorchEffects = CreateDefaultSubobject<USceneComponent>(TEXT("TorchEffects"));
	TorchEffects->SetupAttachment(RootComponent);

	TorchSlowEffect = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("TorchSlowEffect"));
	TorchSlowEffect->SetupAttachment(TorchEffects);
	TorchSlowEffect->SetRelativeLocation(FVector::ZeroVector);
}

void ADarklingCharacter::Die()
{
	Super::Die();
	Destroy();
}

void ADarklingCharacter::SetStatus(EDarklingStatus status) {
	MulticastSetStatus(status);
}

void ADarklingCharacter::MulticastSetStatus_Implementation(EDarklingStatus status)
{
	if (status != EDarklingStatus::SD_None && StatusMaterials.Contains(status)) {
		StatusBillboard->SetMaterial(0, StatusMaterials[status]);
		StatusBillboard->SetHiddenInGame(false);
	}
	else {
		StatusBillboard->SetHiddenInGame(true);
	}
}


void ADarklingCharacter::MulticastPlayAttackParticle_Implementation()
{
	if (AttackParticleEffects) {
		FVector spawnLocation = GetActorLocation() + GetActorForwardVector()*MeleeRange / 2;
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), AttackParticleEffects, spawnLocation);
	}
}

void ADarklingCharacter::BeginPlay()
{
	Super::BeginPlay();
	SpawnController();

	//Get smoke PS
	TArray<UActorComponent*> ParticleSystems = GetComponentsByClass(UParticleSystemComponent::StaticClass());
	for (UActorComponent* component : ParticleSystems) {
		auto particleSystem = Cast<UParticleSystemComponent>(component);
		if (particleSystem) {
			SmokePS = particleSystem;
		}
	}

	TorchEffects->SetVisibility(false, true);

	MulticastSetTorchEffects(IsInLightModeWorldChanger());
}

void ADarklingCharacter::OnResolveAttack()
{
	MulticastPlayAttackParticle();
	if (HasAuthority()) {
		ResolveDarklingAttack();
	}
}

void ADarklingCharacter::OnFinishedAggroAnimation()
{
	CharacterAnimParameters.bIsAggro = false;
	ReplicateAnimParameters();
}

void ADarklingCharacter::SpawnController()
{
	// Make sure this can only be done on the server
	if (GetWorld()->IsServer()) {
		// Spawn an AI to possess this pawn
		auto AI = GetWorld()->SpawnActor<ADarklingAIController>();
		AI->Possess(this);
	}
}

float ADarklingCharacter::GetAttackRange() {
	return MeleeRange;
}

void ADarklingCharacter::ResolveDarklingAttack()
{
	for (TActorIterator<ABaseProjectCharacter> iterator(GetWorld()); iterator; ++iterator) {
		// Check if we are in range and in angle for the attack
		if (IsValidTarget(*iterator)){
			FDamageEvent damageEvent(UDamageType::StaticClass());
			iterator->TakeDamage(AttackDamage, damageEvent, GetController(), this);
		}
	}
}

bool ADarklingCharacter::IsValidTarget(AActor * actor)
{
	bool targetIsPlayer = actor->IsA(ADepressedCharacter::StaticClass()) || actor->IsA(ALightCharacter::StaticClass());

	return Super::IsValidTarget(actor) && targetIsPlayer;
}

void ADarklingCharacter::Stun(float DeltaTime)
{
	CharacterAnimParameters.bIsAttacking = false;
	CharacterAnimParameters.bIsAggro = false;
	CharacterAnimParameters.bIsTakingDamage = true;
	ReplicateAnimParameters();
	if (OnStunned.IsBound()) {
		OnStunned.Broadcast(DeltaTime);
	}
}

void ADarklingCharacter::MulticastSetTorchEffects_Implementation(bool isLit)
{
	if (SmokePS) {
		SmokePS->SetVisibility(!isLit);
	}

	if (TorchEffects) {
		TorchEffects->SetVisibility(isLit, true);
	}
}

void ADarklingCharacter::SetupAnimInstance()
{
	Super::SetupAnimInstance();
	GameAnimInstance->OnAggroAnimCompleted.AddDynamic(this, &ADarklingCharacter::OnFinishedAggroAnimation);
}

void ADarklingCharacter::StartAggroAnim()
{
	CharacterAnimParameters.bIsAggro = true;
	ReplicateAnimParameters();
}
