// Fill out your copyright notice in the Description page of Project Settings.

#include "ActivatorPedestal.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Components/AudioComponent.h"
#include "ColorShiftingComponent.h"
#include "Engine/StaticMeshActor.h"
#include "BaseProjectCharacter.h"
#include "DarklingCharacter.h"
#include "Block.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"


AActivatorPedestal::AActivatorPedestal() {
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Setup basic 
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	Box->SetupAttachment(RootComponent);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);

	ButtonMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ButtonMesh"));
	ButtonMesh->SetupAttachment(RootComponent);
	ButtonMesh->SetMobility(EComponentMobility::Movable);	// So that the mesh can move

	ColorShift = CreateDefaultSubobject<UColorShiftingComponent>(TEXT("ColorShift"));
}

void AActivatorPedestal::BeginPlay()
{
	Super::BeginPlay();
	PrimaryActorTick.bCanEverTick = true;

	Box->OnComponentBeginOverlap.AddDynamic(this, &AActivatorPedestal::OnSteppedOn);
	Box->OnComponentEndOverlap.AddDynamic(this, &AActivatorPedestal::OnSteppedOff);
	
	SetupColorChangerMaterials();
}

void AActivatorPedestal::OnSteppedOn(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (CanInteract(OtherActor)) {
		if (!IsActive()) {
			ShowButtonStateChange(true);
		}
		Activate();
	}
}

void AActivatorPedestal::OnSteppedOff(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	if (CanInteract(OtherActor)) {
		Deactivate();
		if (!IsActive()) {
			ShowButtonStateChange(false);
		}
	}
}

void AActivatorPedestal::ShowButtonStateChange(bool isPressedIn)
{
	FVector NewLocation = isPressedIn ? FVector(0.0f, 0.0f, -20.0f) : FVector(0.0f, 0.0f, 0.0f);
	MoveButtonMesh(NewLocation);
	ColorShift->SetIsInLightMode(isPressedIn);
	
	USoundBase* toggleSound = isPressedIn ? PressedSound : ReleasedSound;
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), toggleSound, GetActorLocation());
}

void AActivatorPedestal::SetupColorChangerMaterials()
{
	TArray<UActorComponent*> AllMeshes = GetComponentsByClass(UStaticMeshComponent::StaticClass());

	for (int i = 0; i < AllMeshes.Num(); i++) {
		auto Mesh = (UStaticMeshComponent*)AllMeshes[i];
		if (Mesh) {
			ColorShift->AddTargetMeshMaterial(Mesh, 0);
		}
	}

	// For all links
	for (AStaticMeshActor* SingleLink : Links) {
		if (SingleLink) {
			auto linkMesh = SingleLink->GetStaticMeshComponent();
			if (linkMesh) {
				ColorShift->AddTargetMeshMaterial(linkMesh, 0);

			}
		}
	}

}

bool AActivatorPedestal::CanInteract(AActor* OtherActor)
{
	auto character = Cast<ABaseProjectCharacter>(OtherActor);
	auto block = Cast<ABlock>(OtherActor);
	auto darkling = Cast<ADarklingCharacter>(OtherActor);
	return ((character || block) && !darkling);
}

void AActivatorPedestal::MoveButtonMesh(FVector NewLocation)
{
	FHitResult * Hit = new FHitResult();
	ETeleportType TeleportType = ETeleportType::None;
	ButtonMesh->SetRelativeLocation(NewLocation, false, Hit, TeleportType);
}

TArray<class AStaticMeshActor*> AActivatorPedestal::GetActivatorLinks()
{
	return Links;
}
	


