// Fill out your copyright notice in the Description page of Project Settings.

#include "LevelCamera.h"
#include "GameFramework/SpringArmComponent.h"
#include "Net/UnrealNetwork.h"
#include "Camera/CameraComponent.h"
#include "EngineUtils.h"
#include "DepressedCharacter.h"
#include "LightCharacter.h"

// Sets default values
ALevelCamera::ALevelCamera()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Use a spring arm to give the camera smooth, natural-feeling motion.
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraAttachmentArm"));
	SpringArmComponent->SetupAttachment(RootComponent);
	SpringArmComponent->RelativeRotation = FRotator(-45.f, 0.f, 0.f);
	SpringArmComponent->TargetArmLength = 400.0f;
	SpringArmComponent->bEnableCameraLag = true;
	SpringArmComponent->CameraLagSpeed = 3.0f;
	SpringArmComponent->bDoCollisionTest = false;

	// Create a camera and attach to our spring arm
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SceneCamera"));
	CameraComponent->SetupAttachment(SpringArmComponent, USpringArmComponent::SocketName);

	RootComponent = SpringArmComponent;
}

// Called when the game starts or when spawned
void ALevelCamera::BeginPlay()
{
	Super::BeginPlay();
	CameraVignetteAmount = NearCameraVignette;
	if (VignetteIntensityRateMultiplier <= 0) {
		VignetteIntensityRateMultiplier = 0.3;
	}
	InitializePerspectiveTrackingData();
}

// Called every frame
void ALevelCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!PrimaryController) {
		return;
	}

	FVector targetPosition = GetTargetsCenterOfMass();
	float TargetArmLength = ComputeTargetArmLength(targetPosition);

	APawn* targetPawn = PrimaryController->GetPawn();
	if ((TargetArmLength >= MaximumArmLength) && targetPawn) {
		TargetArmLength = MaximumArmLength;
		targetPosition = targetPawn->GetActorLocation();
		if (!bAreDistanced) {
			bAreDistanced = true;
			bIsInterpolating = true;
		}
	}
	else {
		if (bAreDistanced) {
			bAreDistanced = false;
			bIsInterpolating = true;
		}
	}

	SpringArmComponent->TargetArmLength = FMath::Lerp(SpringArmComponent->TargetArmLength, TargetArmLength, FMath::Clamp(ArmLengthSpeed*DeltaTime, 0.f, 1.f));
	this->SetActorLocation(targetPosition);

	// Adjust vignette according to distance
	if (bIsInterpolating) {
		CameraVignetteAmount += bAreDistanced ? DeltaTime*VignetteIntensityRateMultiplier : -DeltaTime*VignetteIntensityRateMultiplier;
		CameraVignetteAmount = FMath::Clamp(CameraVignetteAmount, NearCameraVignette, DistancedCameraVignette);

		if (CameraComponent) {
			CameraComponent->PostProcessSettings.VignetteIntensity = CameraVignetteAmount;
		}
		if ((bAreDistanced && (CameraVignetteAmount == DistancedCameraVignette)) || (!bAreDistanced && (CameraVignetteAmount == NearCameraVignette))) {
			bIsInterpolating = false;
		}
	}
}

void ALevelCamera::SetPrimaryController(AController * controller)
{
	if (!PrimaryController) {
		PrimaryController = controller;
	}
}

FVector ALevelCamera::GetTargetsCenterOfMass()
{
	FVector sum = FVector::ZeroVector;
	int actorCount = 0;
	float zDifference = 0;

	for (TActorIterator<ADepressedCharacter> itr(GetWorld()); itr; ++itr) {
		FVector location = itr->GetActorLocation();
		sum += location;
		zDifference = location.Z;
		actorCount++;
	}
	for (TActorIterator<ALightCharacter> itr(GetWorld()); itr; ++itr) {
		FVector location = itr->GetActorLocation();
		sum += location;
		zDifference -= location.Z;
		actorCount++;
	}

	currentZDifference = actorCount > 1 ? FMath::Abs(zDifference) : 0.0f;

	return actorCount > 0 ? sum / actorCount : GetActorLocation();
}

float ALevelCamera::ComputeTargetArmLength(const FVector & centerOfMass)
{
	float TargetLength = 0;

	for (TActorIterator<ADepressedCharacter> itr(GetWorld()); itr; ++itr) {
		TargetLength = FMath::Max(TargetLength, GetLargestArmLengthForActor(*itr, centerOfMass));
	}
	for (TActorIterator<ALightCharacter> itr(GetWorld()); itr; ++itr) {
		TargetLength = FMath::Max(TargetLength, GetLargestArmLengthForActor(*itr, centerOfMass));
	}

	TargetLength = FMath::Max(MinimumArmLength + (MaximumArmLength - MinimumArmLength)* currentZDifference / MaximumZDifference, TargetLength);

	return FMath::Clamp(TargetLength, MinimumArmLength, MaximumArmLength);
}

float ALevelCamera::GetLargestArmLengthForActor(AActor * actor, const FVector & centerOfMass)
{
	// Calculate horizontal angle camera length
	FVector cameraForward = CameraComponent->GetForwardVector();
	FVector predictedCameraPosition = centerOfMass - (cameraForward*SpringArmComponent->TargetArmLength);
	FVector distanceVector = actor->GetActorLocation() - predictedCameraPosition;

	float cameraPlaneDistance = distanceVector.ProjectOnTo(cameraForward).Size();

	float horizontalDistance = distanceVector.ProjectOnTo(CameraComponent->GetRightVector()).Size();
	float targetHorizontalArmLength = FMath::Abs(horizontalDistance/TargetMaximumYawTan);


	float verticalDistance = distanceVector.ProjectOnTo(CameraComponent->GetUpVector()).Size();
	float targetVerticalArmLength = FMath::Abs(verticalDistance / TargetMaximumPitchTan);

	return FMath::Max(targetHorizontalArmLength, targetVerticalArmLength);
}

void ALevelCamera::InitializePerspectiveTrackingData()
{
	TargetMaximumYawTan = FMath::Abs(MaxHorNormalizedDistanceFromCenter * FMath::Tan(CameraComponent->FieldOfView / 2));
	float vertFOV = 2 * FMath::Atan(FMath::Tan(CameraComponent->FieldOfView / 2)*(1 / CameraComponent->AspectRatio));
	TargetMaximumPitchTan = FMath::Abs(MaxVertNormalizedDistanceFromCenter * FMath::Tan(vertFOV / 2));
}
