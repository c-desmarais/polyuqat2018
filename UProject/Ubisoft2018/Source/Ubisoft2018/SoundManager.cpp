// Fill out your copyright notice in the Description page of Project Settings.

#include "SoundManager.h"


// Sets default values
ASoundManager::ASoundManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
	AudioComponent->SetupAttachment(RootComponent);
}

void ASoundManager::BeginPlay()
{
	Super::BeginPlay();
	AudioComponent->OnAudioPlaybackPercent.AddDynamic(this, &ASoundManager::OnAudioPlayback);

	if (LevelBeginSound) {
		AudioComponent->SetSound(LevelBeginSound);
		AudioComponent->Play();
	}
}

void ASoundManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASoundManager::OnAudioPlayback(const USoundWave* PlayingWave, const float PlaybackPercent)
{
	if (PlaybackPercent > 0.99f && LoopingSound && !IsCurrentSound(LoopingSound)) {
		PlaySound(LoopingSound);
	}
}

bool ASoundManager::IsCurrentSound(USoundBase* sound) {
	return sound == CurrentSound;
}

void ASoundManager::FadeInSound(USoundBase* sound) {
	FadeInSound(sound, FadeDuration);
}

void ASoundManager::FadeInSound(USoundBase * sound, float fadeInTime)
{
	if (!sound) return;

	LoopingSound = nullptr;
	CurrentSound = sound;
	AudioComponent->SetSound(CurrentSound);
	AudioComponent->FadeIn(fadeInTime, 1.0F, 0.0F);
}

void ASoundManager::FadeOutSound() {
	AudioComponent->FadeOut(FadeDuration, 0.0f);
	CurrentSound = nullptr;
}

void ASoundManager::PlaySound(USoundBase* sound) {
	if (!sound) return; 
	LoopingSound = nullptr;
	CurrentSound = sound;
	AudioComponent->SetSound(CurrentSound);
	AudioComponent->Play();
}

void ASoundManager::StopSound() {
	AudioComponent->Stop();
	CurrentSound = nullptr;
}

void ASoundManager::FadeInInitSoundAndPlayOtherSoundInLoop(USoundBase* initSound, USoundBase* loopingSound) {
	CurrentSound = initSound;
	LoopingSound = loopingSound;
	AudioComponent->SetSound(CurrentSound);
	AudioComponent->FadeIn(FadeDuration, 1.0F);
}

void ASoundManager::PlayInitSoundAndPlayOtherSoundInLoop(USoundBase* initSound, USoundBase* loopingSound) {
	CurrentSound = initSound;
	LoopingSound = loopingSound;
	AudioComponent->SetSound(CurrentSound);
	AudioComponent->Play();
}

void ASoundManager::SetLastActivatedTrigger(ABaseMusicTrigger* MusicTrigger) {
	LastActivatedMusicTrigger = MusicTrigger;
}

ABaseMusicTrigger* ASoundManager::GetLastActivatedTrigger() {
	return LastActivatedMusicTrigger;
}