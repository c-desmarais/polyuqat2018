// Fill out your copyright notice in the Description page of Project Settings.

#include "Wall.h"
#include "ColorShiftingComponent.h"
#include "Activator.h"
#include "Components/AudioComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

// Sets default values
AWall::AWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	nbOfActivatedActivators = 0;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	MeshArch = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshArch"));
	MeshArch->SetupAttachment(RootComponent);

	MeshDoor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshDoor"));
	MeshDoor->SetupAttachment(RootComponent);

	ColorShift = CreateDefaultSubobject<UColorShiftingComponent>(TEXT("ColorShift"));
}

// Called when the game starts or when spawned
void AWall::BeginPlay()
{
	Super::BeginPlay();
	ListenToActivators();
	ColorShift->AddTargetMeshMaterial(MeshArch, 0);
}

// Called every frame
void AWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWall::ListenToActivators()
{
	for (AActivator* activator : Activators) {
		activator->OnActivate.AddDynamic(this, &AWall::DeActivateWall);
		activator->OnDeactivate.AddDynamic(this, &AWall::DeactivateActivator);
	}
}

void AWall::DeactivateActivator() {
	nbOfActivatedActivators--;
	if (IsPuzzleWall && nbOfActivatedActivators <= 0) {
		ActivateWall();
	}

}

void AWall::DeActivateWall() {
	nbOfActivatedActivators++;
	if(nbOfActivatedActivators >= Activators.Num()) {
		if (IsActivated) {
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), DeactivateSound, GetActorLocation());
		}
		IsActivated = false;
		MeshDoor->SetVisibility(IsActivated);
		MeshDoor->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		ColorShift->SetIsInLightMode(!IsActivated);
	}
}

void AWall::ActivateWall()
{
	if (!IsActivated) {
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), DeactivateSound, GetActorLocation());
	}
	IsActivated = true;
	MeshDoor->SetVisibility(IsActivated);
	MeshDoor->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	ColorShift->SetIsInLightMode(!IsActivated);
}

bool AWall::GetIsActivated() {
	return IsActivated;
}

