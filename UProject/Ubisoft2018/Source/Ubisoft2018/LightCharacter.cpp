// Fill out your copyright notice in the Description page of Project Settings.

#include "LightCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "EngineDefines.h"
#include "Engine.h"
#include "WaveProjectile.h"
#include "Components/SphereComponent.h"
#include "Torch.h"

// Sets default values
ALightCharacter::ALightCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AimVisualsRoot = CreateDefaultSubobject<USceneComponent>(TEXT("AimVisuals"));
	AimVisualsRoot->SetupAttachment(RootComponent);

	bReplicates = true;
}

void ALightCharacter::SetupAnimInstance()
{
	Super::SetupAnimInstance();
	GameAnimInstance->OnJumpAnimCompleted.AddDynamic(this, &ALightCharacter::OnJumpCompleted);
}

// Called every frame
void ALightCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

// Called to bind functionality to input
void ALightCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

float ALightCharacter::TakeDamage(float DamageAmount, FDamageEvent const & DamageEvent, AController * EventInstigator, AActor * DamageCauser)
{
	// The player is immune to damage when he's already taking damage
	if (CharacterAnimParameters.bIsTakingDamage == true)
	{
		return 0.0f;
	}
	else
	{
		return Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	}
}

bool ALightCharacter::CanAttack()
{
	return Super::CanAttack() && !bAttackOnCooldown;
}

void ALightCharacter::ServerEnqueueAttackRequest_Implementation(FVector AttackDirection)
{
	if (CanAttack()) {
		Super::ServerEnqueueAttackRequest_Implementation(AttackDirection);

		MulticastSetAimDecalVisibility(false);
		if (!GetWorldTimerManager().IsTimerActive(AttackCoolDownTimerHandle)) {
			GetWorldTimerManager().SetTimer(AttackCoolDownTimerHandle, this, &ALightCharacter::MulticastEnableAttack, AttackCoolDown, false);
		}

		FVector const AimVect = AdjustFacingVector(GetActorForwardVector());
		MulticastSpawnWaveProjectile(GetActorLocation(), AimVect);
		bAttackOnCooldown = true;
	}
}

bool ALightCharacter::ServerEnqueueAttackRequest_Validate(FVector AttackDirection)
{
	return true;
}

void ALightCharacter::MulticastSpawnWaveProjectile_Implementation(FVector Location, FVector LaunchDirection)
{
	UWorld* const World = GetWorld();
	if (World && ProjectileClass) {
		//LightCharacter will be responsible for damage done by the projectile
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.Owner = this;
		SpawnParameters.Instigator = Instigator;

		// Spawn the projectile
		AWaveProjectile* const Projectile = World->SpawnActor<AWaveProjectile>(ProjectileClass, Location, LaunchDirection.Rotation() , SpawnParameters);
		if (Projectile)
		{
			Projectile->Launch(LaunchDirection);
		}
	}
}

void ALightCharacter::MulticastEnableAttack_Implementation()
{
	EnableAttack();
}

void ALightCharacter::EnableAttack()
{
	bAttackOnCooldown = false;

	SetAimDecalVisibility(true);
}

void ALightCharacter::MulticastSetAimDecalVisibility_Implementation(bool visible)
{
	SetAimDecalVisibility(visible);
}

void ALightCharacter::SetAimDecalVisibility(bool visible)
{
	AimVisualsRoot->SetHiddenInGame(!visible, true);
}

bool ALightCharacter::ServerLightTorch_Validate(ATorch* NearestTorch)
{
	return true;
}

void ALightCharacter::ServerLightTorch_Implementation(ATorch* NearestTorch)
{
	MulticastLightTorch(NearestTorch);
}

void ALightCharacter::MulticastLightTorch_Implementation(ATorch* Torch)
{
	if (Torch) {
		Torch->LightTorch();
	}
}

void ALightCharacter::ServerDispatchLightAction_Implementation()
{
	//Verify if next to a torch
	if (NearbyTorch && !NearbyTorch->GetIsLit()) {
		ServerLightTorch(NearbyTorch);
	}
	else {
		ServerJump();
	}
}

bool ALightCharacter::ServerDispatchLightAction_Validate()
{
	return true;
}

void ALightCharacter::OnJumpCompleted()
{
	CharacterAnimParameters.bIsJumping = false;
	ReplicateAnimParameters();
}

bool ALightCharacter::ServerJump_Validate()
{
	return true;
}

void ALightCharacter::ServerJump_Implementation()
{
	CharacterAnimParameters.bIsJumping = true;
	ReplicateAnimParameters();
	MulticastJump();
}

void ALightCharacter::MulticastJump_Implementation()
{
	Jump();
}

void ALightCharacter::SetNearbyTorch(ATorch* torch) {
	NearbyTorch = torch;
}


