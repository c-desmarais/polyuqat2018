// Fill out your copyright notice in the Description page of Project Settings.

#include "WallTrigger.h"
#include "Wall.h"
#include "LightCharacter.h"
#include "DepressedCharacter.h"
#include "Engine.h"
#include "Components/BoxComponent.h"



// Sets default values
AWallTrigger::AWallTrigger()
{
	TriggerZone = CreateDefaultSubobject<UBoxComponent>(TEXT("TriggerZone"));
	TriggerZone->SetupAttachment(RootComponent);
	RootComponent = TriggerZone;

	TriggerZone->OnComponentBeginOverlap.AddDynamic(this, &AWallTrigger::OnOverlapBegin);

}

// Called when the game starts or when spawned
void AWallTrigger::BeginPlay()
{
	Super::BeginPlay();
	
}

void AWallTrigger::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	
	if (OtherActor->IsA(ALightCharacter::StaticClass()))
	{
		bLightCharacterPassed = !bLightCharacterPassed;
	}
	else if (OtherActor->IsA(ADepressedCharacter::StaticClass()))
	{
		bDepressedCharacterPassed = !bDepressedCharacterPassed;
	}

	if (bLightCharacterPassed && bDepressedCharacterPassed) {
		if (Wall) {
			Wall->ActivateWall();
			SetActorEnableCollision(false);
		}
	}
}

