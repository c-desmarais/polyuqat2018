// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "VisibilityCounterComponent.generated.h"

/*
Scene component whose children are visible when the counter is above 0 and invisible when it is equal or below 0
*/
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UBISOFT2018_API UVisibilityCounterComponent : public USceneComponent
{
	GENERATED_BODY()

public:	

	int GetCount();

	void SetCount(int count);
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION(NetMulticast, Reliable)
		void MulticastSetHiddenInGame(bool hidden);

	UPROPERTY(EditAnywhere)
	int Counter = 0;
	
};
