// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WorldChangerVolumeTrigger.generated.h"

class UBoxComponent;
class AWorldChangerVolume;

UCLASS()
class UBISOFT2018_API AWorldChangerVolumeTrigger : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWorldChangerVolumeTrigger();

protected:

	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		UBoxComponent* TriggerZone;

	UPROPERTY(EditAnywhere)
		AWorldChangerVolume* WorldChangeVolume = nullptr;

	
	
};
