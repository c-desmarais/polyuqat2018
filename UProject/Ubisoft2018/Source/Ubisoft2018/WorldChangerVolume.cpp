// Fill out your copyright notice in the Description page of Project Settings.

#include "WorldChangerVolume.h"
#include "Kismet/GameplayStatics.h"
#include "Components/StaticMeshComponent.h"
#include "ColorShiftingComponent.h"
#include "ActivatorPedestal.h"
#include "SoundManager.h"
#include "Engine/StaticMeshActor.h"
#include "BaseProjectCharacter.h"
#include "DepressedCharacter.h"
#include "DarklingCharacter.h"
#include "Torch.h"
#include "Wall.h"
#include "MonsterSpawner.h"

AWorldChangerVolume::AWorldChangerVolume()
{
	ColorShift = CreateDefaultSubobject<UColorShiftingComponent>(TEXT("ColorShift"));
}

void AWorldChangerVolume::BeginPlay()
{
	Super::BeginPlay();
	PrimaryActorTick.bCanEverTick = true;

	SetupWorldChanger();

	OnActorBeginOverlap.AddDynamic(this, &AWorldChangerVolume::OnOverlapBegin);
	OnActorEndOverlap.AddDynamic(this, &AWorldChangerVolume::OnOverlapEnd);
}

void AWorldChangerVolume::SetupWorldChanger()
{
	TArray<AActor*> ActorArray;
	GetOverlappingActors(ActorArray);
	for (AActor* Actor : ActorArray) {
		if (Actor) {
			if (IsChangeable(Actor)) {
				//Find all meshes
				TArray<UActorComponent*> AllMeshes = Actor->GetComponentsByClass(UStaticMeshComponent::StaticClass());
				for (int i = 0; i < AllMeshes.Num(); i++) {
					auto Mesh = (UStaticMeshComponent*)AllMeshes[i];
					if (Mesh) {
						for (int i = 0; i <= Mesh->GetNumMaterials(); i++) {
							ColorShift->AddTargetMeshMaterial(Mesh, i);
						}
					}
				}
			}
		}
	}
}

void AWorldChangerVolume::ChangeColorMode(bool isInLightMode)
{	
	ColorShift->SetIsInLightMode(isInLightMode);
}

void AWorldChangerVolume::ChangeWorld(bool isInLightMode)
{
	if ((!isInLightMode && bIsLitByMultipleTorches) || (bIsInLightMode && isInLightMode)) {
		return;
	}

	bIsInLightMode = isInLightMode;
	ChangeColorMode(isInLightMode);
	AffectCharacters();
}

void AWorldChangerVolume::AffectCharacters()
{
	TArray<AActor*> ActorArray;
	GetOverlappingActors(ActorArray, ABaseProjectCharacter::StaticClass());

	for (AActor* Actor : ActorArray) {
		if (Actor) {
			auto gloom = Cast<ADepressedCharacter>(Actor);
			if (gloom) {
				AffectDepressedCharacter(gloom, !bIsInLightMode);
			}
			else {
				auto darkling = Cast<ADarklingCharacter>(Actor);
				if (darkling){
					AffectDarklingCharacter(darkling, !bIsInLightMode);
				}
			}
		}
	}
}

void AWorldChangerVolume::AffectDepressedCharacter(ADepressedCharacter* character, bool isCharacterAffected)
{
	if (character) {
		if (!isCharacterAffected) {
			AffectedCharacters.Add(character);
			character->AddSpeedModifier(GloomSpeedRatio);
			character->ActivateTorchEffects(true);
		}
		else {
			if (AffectedCharacters.Contains(character)) {
				character->RemoveSpeedModifier(GloomSpeedRatio);
				AffectedCharacters.RemoveSingle(character);
				character->ActivateTorchEffects(false);
			}
		}
	}
}

void AWorldChangerVolume::AffectDarklingCharacter(ADarklingCharacter* character, bool isCharacterAffected)
{
	if (character) {
		if (!isCharacterAffected) {
			AffectedCharacters.Add(character);
			character->AddSpeedModifier(DarklingSpeedRatio);
			//Deactivate smoke particles
			character->MulticastSetTorchEffects(true);
		}
		else {
			if (AffectedCharacters.Contains(character)) {
				character->RemoveSpeedModifier(DarklingSpeedRatio);
				AffectedCharacters.RemoveSingle(character);
				//Activate smoke particles
				character->MulticastSetTorchEffects(false);
			}
		}
	}
}

void AWorldChangerVolume::OnOverlapBegin(AActor* MyOverlappedActor, AActor* OtherActor)
{
	auto darkling = Cast<ADarklingCharacter>(OtherActor);
	if (darkling) {
		if (bIsInLightMode) {
			AffectDarklingCharacter(darkling, false);
		}
	}
	else {
		auto depressed = Cast<ADepressedCharacter>(OtherActor);
		if (depressed) {
			if (bIsInLightMode) {
				AffectDepressedCharacter(depressed, false);
			}
		}
		
	}
	
}

void AWorldChangerVolume::OnOverlapEnd(AActor* MyOverlappedActor, AActor* OtherActor)
{
	auto darkling = Cast<ADarklingCharacter>(OtherActor);
	if (darkling) {
		if (bIsInLightMode) {
			AffectDarklingCharacter(darkling, true);
		}
	}
	else {
		auto depressed = Cast<ADepressedCharacter>(OtherActor);
		if (depressed) {
			if (bIsInLightMode) {
				AffectDepressedCharacter(depressed, true);
			}
		}
	}
}

bool AWorldChangerVolume::IsChangeable(AActor* Actor)
{
	auto activator = Cast<AActivatorPedestal>(Actor);
	if (activator) {
		return false;
	}
	else {
		auto block = Cast<ABlock>(Actor);
		if (block) {
			return false;
		}
		else {
			auto torch = Cast<ATorch>(Actor);
			if (torch) {
				return false;
			}
			else {
				auto gate = Cast<AWall>(Actor);
				if (gate) {
					return false;
				}
				else {
					auto monsterSpawner = Cast<AMonsterSpawner>(Actor);
					if (monsterSpawner) {
						return false;
					}
					else {
						return true;
					}
				}
			}
		}
	}
}

void AWorldChangerVolume::DestroyAllDarklingsInArea()
{
	TArray<AActor*> ActorArray;
	GetOverlappingActors(ActorArray, ABaseProjectCharacter::StaticClass());

	for (AActor* Actor : ActorArray) {
		if (Actor) {
			auto darkling = Cast<ADarklingCharacter>(Actor);
			if (darkling) {
				darkling->Die();
			}
		}
	}
}

void AWorldChangerVolume::AddLitTorch()
{
	if (++NbOfLitTorches >= 2) {
		bIsLitByMultipleTorches = true;
	}
}

void AWorldChangerVolume::RemoveLitTorch()
{
	if (--NbOfLitTorches <= 0) {
		bIsLitByMultipleTorches = false;
	}
}

bool AWorldChangerVolume::IsInLightMode()
{
	return bIsInLightMode;
}