// Fill out your copyright notice in the Description page of Project Settings.

#include "VisibilityCounterComponent.h"

int UVisibilityCounterComponent::GetCount()
{
	return Counter;
}

void UVisibilityCounterComponent::SetCount(int count)
{
	bool wasAboveZero = Counter > 0;
	Counter = count;
	if(wasAboveZero != Counter > 0){
		// State changed, replicate new visibility
		MulticastSetHiddenInGame(Counter <= 0);
	}
}

// Called when the game starts
void UVisibilityCounterComponent::BeginPlay()
{
	Super::BeginPlay();

	if (Counter == 0) {
		SetHiddenInGame(true, true);
	}
}

void UVisibilityCounterComponent::MulticastSetHiddenInGame_Implementation(bool hidden)
{
	SetHiddenInGame(hidden, true);
}

