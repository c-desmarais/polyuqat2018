// Fill out your copyright notice in the Description page of Project Settings.

#include "Sword.h"
#include "EngineUtils.h"
#include "Runtime/Engine/Classes/Components/AudioComponent.h"

// Sets default values
ASword::ASword()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	MeshSword = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshSword"));
	MeshSword->SetupAttachment(RootComponent);

	SwordDragEffects = CreateDefaultSubobject<USceneComponent>(TEXT("SwordDragEffects"));
	SwordDragEffects->SetupAttachment(RootComponent);

	DragSoundComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("SwordDragSound"));
	DragSoundComponent->SetupAttachment(RootComponent);

	bReplicates = false;
	bReplicateMovement = false;
}

void ASword::SetSwordDragEffectsHidden(bool IsHidden)
{
	if (SwordDragEffects)
	{
		SwordDragEffects->SetHiddenInGame(IsHidden, true);
	}
	if (IsHidden) {
		DragSoundComponent->FadeOut(0.1f, 0.0f);
	}
	else {
		DragSoundComponent->FadeIn(0.2f);
	}
}

