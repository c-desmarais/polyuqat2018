// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "UbisoftAnimInstance.h"
#include "BaseProjectCharacter.generated.h"


class USpringArmComponent;


/** Delegate for notification of the death */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FDeathSignature, ACharacter*, DeadCharacter, bool, bKilled);


UCLASS()
class UBISOFT2018_API ABaseProjectCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	virtual void Restart() override;

	// Sets default values for this character's properties
	ABaseProjectCharacter();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override; 

	virtual void Move(FVector MoveDelta, float DeltaTime);

	virtual void Attack(FVector AttackDirection);

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser);

	virtual void Die();

	UPROPERTY(BlueprintAssignable)
	FDeathSignature OnDeath;

	virtual void PushAction();

	UFUNCTION(Server, Reliable, WithValidation)
	virtual void ServerDispatchLightAction();

	UFUNCTION(BlueprintCallable)
		float GetHealth();

	void AddSpeedModifier(float ratio);

	void RemoveSpeedModifier(float ratio);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual bool CanAttack();

	virtual FVector AdjustFacingVector(FVector AttackDirection);

	UFUNCTION(Server, Reliable, WithValidation)
	virtual void ServerEnqueueAttackRequest(FVector AttackDirection);

	virtual void SetupAnimInstance();

	virtual void UpdateAnimationInstanceValues();
	
	void ReplicateAnimParameters();

	void ReplicateAnimParameters(FVector ActorFacing);

	UFUNCTION(NetMulticast, Reliable)
	virtual void MulticastUpdateAnimParameters(FVector ActorFacing, FAnimParameters parameters);

	UFUNCTION()
	virtual void OnResolveAttack();

	virtual bool IsValidTarget(AActor* actor);
	
	UFUNCTION()
	virtual void OnStartMoving();

	UFUNCTION()
	virtual void OnStopMoving();

	UFUNCTION()
	virtual void OnFinishedAttackAnimation();

	UFUNCTION()
	virtual void OnFinishedDeathAnimation();

	UFUNCTION()
	virtual void OnFinishedHurtAnimation();

	// Set movement speed of component to speed from modifiers
	void UpdateToCurrentSpeed();

	void CreateDynamicMeshInstances();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerActivateHurtEffects(float Hurt);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastChangeMaterial(float Hurt);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastPlayHurtParticleEffect();

	UFUNCTION(NetMulticast, Reliable)
	void MulticastPlayTakeDamageSound();

	UFUNCTION(NetMulticast, Reliable)
	void MulticastPlayDeathSound();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerPlayDeathSound();

	UFUNCTION(NetMulticast, Reliable)
	void MulticastPlayDeathParticle();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerPlayDeathParticle();

	UFUNCTION()
	bool IsInLightModeWorldChanger();

	UPROPERTY(EditAnywhere, Category = "BaseProjectCharacter|MeleeAttack")
		float AttackAngle = 45.0f;

	UPROPERTY(EditAnywhere, Category = "BaseProjectCharacter|MeleeAttack")
		float MeleeRange = 200.0f;

	UPROPERTY(EditAnywhere, Category = "BaseProjectCharacter|MeleeAttack")
		float AttackDamage = 1.0f;

	UPROPERTY()
	float MovementSpeed = 80.0f;

	FAnimParameters CharacterAnimParameters;

	UUbisoftAnimInstance * GameAnimInstance;

	UPROPERTY(EditAnywhere)
	UParticleSystem* TakeDamageParticleEffects;

	UPROPERTY(EditAnywhere)
	UParticleSystem* DeathParticleEffects;

	UPROPERTY()
		TArray<float> SpeedModifiers;

	UPROPERTY()
	TArray<class UMaterialInstanceDynamic*> DynamicMaterials;

	float Hurt = 0.0f;

	bool bHasRegularMaterial = true;

	UPROPERTY(EditAnywhere)
	USceneComponent* SoundEffects;

	UPROPERTY(EditAnywhere)
	class USoundCue* DeathSound = nullptr;

	UPROPERTY(EditAnywhere)
	UAudioComponent* TakeDamageAudioComponent;

	//Will lerp from 6 to -1 to gradually appear
	float DissolveAmount = 6;

	bool bIsSpawning = false;

	UPROPERTY(EditAnywhere)
	float DissolveRateMultiplier = 4.0f;

private:	
	UFUNCTION()
	void SetIsMoving(bool IsMoving);

	UPROPERTY()
	bool bIsMoving = false;

	UPROPERTY(EditAnywhere, Replicated)
	float Health = 10.0f;

	UPROPERTY(EditAnywhere)
	class UVisibilityCounterComponent * TargetEffectRoot;
};
