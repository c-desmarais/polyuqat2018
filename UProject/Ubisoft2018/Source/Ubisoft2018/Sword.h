// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Sword.generated.h"

UCLASS()
class UBISOFT2018_API ASword : public AActor
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	class USceneComponent* Root;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* MeshSword;

	UPROPERTY(EditAnywhere)
	class USceneComponent* SwordDragEffects;

	UPROPERTY(EditAnywhere)
	class UAudioComponent* DragSoundComponent;

public:	
	// Sets default values for this actor's properties
	ASword();

	UFUNCTION()
	void SetSwordDragEffectsHidden(bool Activate);
};
