// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/AudioComponent.h"
#include "SoundManager.generated.h"

class ABaseMusicTrigger;

UCLASS()
class UBISOFT2018_API ASoundManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASoundManager();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void FadeInSound(USoundBase* sound);

	void FadeInSound(USoundBase* sound, float fadeInTime);

	void FadeOutSound();

	void FadeInInitSoundAndPlayOtherSoundInLoop(USoundBase* initSound, USoundBase* loopingSound);

	void PlayInitSoundAndPlayOtherSoundInLoop(USoundBase* initSound, USoundBase* loopingSound);

	void PlaySound(USoundBase* sound);

	void StopSound();

	ABaseMusicTrigger* GetLastActivatedTrigger();

	void SetLastActivatedTrigger(ABaseMusicTrigger* MusicTrigger);

	bool IsCurrentSound(USoundBase* sound);

	UPROPERTY(EditAnywhere)
		USoundBase* LevelBeginSound;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	USoundBase* CurrentSound;
	USoundBase* LoopingSound;
	ABaseMusicTrigger* LastActivatedMusicTrigger;

	UPROPERTY(EditAnywhere)
		float FadeDuration = 5.0f;

	UPROPERTY()
		UAudioComponent* AudioComponent;

	UFUNCTION()
		void OnAudioPlayback(const USoundWave* PlayingWave, const float PlaybackPercent);
	
	
};
