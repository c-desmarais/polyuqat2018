// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseProjectPlayerController.h"
#include "Net/UnrealNetwork.h"
#include "EngineUtils.h"
#include "LevelCamera.h"
#include "BaseProjectCharacter.h"
#include "Engine.h"
#include "InGameWidgetsContainer.h"

const FName ABaseProjectPlayerController::MoveForwardBinding("MoveForward");
const FName ABaseProjectPlayerController::MoveSidewaysBinding("MoveRight");

ABaseProjectPlayerController::ABaseProjectPlayerController(const FObjectInitializer & ObjectInitializer)
	:Super(ObjectInitializer)
{
	bReplicates = true;

	bAutoManageActiveCameraTarget = false;
}

void ABaseProjectPlayerController::BeginPlay()
{
	// Tell player controller to see through level camera
	for (TActorIterator<ALevelCamera> Itr(GetWorld()); Itr; ++Itr)
	{
		ClientSetViewTarget(*Itr);
		Itr->SetPrimaryController(this);
	}
}

// Source: Storm Trooper by Camille
void ABaseProjectPlayerController::Tick(float deltaTime)
{
	if (GetPawn() == nullptr)
		return;
	
	FVector MovementDirection = GetInputDirectionVector().GetSafeNormal(AxisDetectionThreshold);

	((ABaseProjectCharacter*)GetPawn())->Move(MovementDirection, deltaTime);

}

void ABaseProjectPlayerController::ClientShowVictoryScreen_Implementation()
{
	GetWidgetsContainer()->OpenWidget(EWidgetEnum::WE_Credits);
}

// Source: Storm Trooper by Camille
void ABaseProjectPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	// Action Binding
	InputComponent->BindAction("Jump", IE_Pressed, this, &ABaseProjectPlayerController::Jump);
	InputComponent->BindAction("Jump", IE_Released, this, &ABaseProjectPlayerController::StopJumping);

	InputComponent->BindAction("Attack", IE_Pressed, this, &ABaseProjectPlayerController::Attack);

	InputComponent->BindAction("Push", IE_Pressed, this, &ABaseProjectPlayerController::Push);

	InputComponent->BindAction("PauseMenu", IE_Released, this, &ABaseProjectPlayerController::PauseMenu);

	// Movement input
	InputComponent->BindAxis(MoveForwardBinding);
	InputComponent->BindAxis(MoveSidewaysBinding);
}

void ABaseProjectPlayerController::ClientWaitForOtherPlayer_Implementation()
{
	GetWidgetsContainer()->OpenWidget(EWidgetEnum::WE_Waiting);
}

void ABaseProjectPlayerController::ClientOtherPlayerJoined_Implementation()
{
	GetWidgetsContainer()->CloseWidget(EWidgetEnum::WE_Waiting);
	GetWidgetsContainer()->OpenWidget(EWidgetEnum::WE_HUD);
	GetWidgetsContainer()->OpenWidget(EWidgetEnum::WE_Joined);
}

FVector ABaseProjectPlayerController::GetInputDirectionVector()
{
	// Get the camera's forward and side
	FVector Forward = GetViewTarget()->GetActorForwardVector() * GetInputAxisValue(MoveForwardBinding);
	FVector Sideways = GetViewTarget()->GetActorRightVector() * GetInputAxisValue(MoveSidewaysBinding);

	return Forward + Sideways;
}

void ABaseProjectPlayerController::Jump()
{
	auto CharacterPawn = (ABaseProjectCharacter*)GetPawn();
	if (CharacterPawn)
	{
		CharacterPawn->ServerDispatchLightAction();
	}
}

void ABaseProjectPlayerController::StopJumping()
{
	auto CharacterPawn = (ABaseProjectCharacter*)GetPawn();
	if (CharacterPawn)
	{
		CharacterPawn->StopJumping();
	}
}

void ABaseProjectPlayerController::Attack()
{
	auto CharacterPawn = (ABaseProjectCharacter*)GetPawn();
	if (CharacterPawn)
	{
		CharacterPawn->Attack(GetInputDirectionVector());
	}
}


void ABaseProjectPlayerController::Push()
{
	auto CharacterPawn = (ABaseProjectCharacter*)GetPawn();
	if (CharacterPawn)
	{
		CharacterPawn->PushAction();
	}
}

void ABaseProjectPlayerController::PauseMenu() 
{
	if (!GetWidgetsContainer()->IsWidgetOpen(EWidgetEnum::WE_Pause)) {
		GetWidgetsContainer()->OpenWidget(EWidgetEnum::WE_Pause);
	}
	else {
		GetWidgetsContainer()->CloseWidget(EWidgetEnum::WE_Pause);
	}
}

bool ABaseProjectPlayerController::ServerShowMainMenu_Validate() {
	return true;
}

void ABaseProjectPlayerController::ServerShowMainMenu_Implementation() {
	ConsoleCommand(TEXT("open MainMenu"), true); 
}

UInGameWidgetsContainer * ABaseProjectPlayerController::GetWidgetsContainer()
{
	if (!InGameWidgetsContainer) {
		InGameWidgetsContainer = NewObject<UInGameWidgetsContainer>(this, InGameWidgetsContainerClass);
		InGameWidgetsContainer->SetParentController(this);
	}
	return InGameWidgetsContainer;
}
