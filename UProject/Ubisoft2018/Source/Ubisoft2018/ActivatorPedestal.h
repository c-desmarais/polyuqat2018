// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Activator.h"
#include "ActivatorPedestal.generated.h"

class UColorShiftingComponent;

/**
 * Pedestal that activates whatever listens to it when stepped on 
 */
UCLASS()
class UBISOFT2018_API AActivatorPedestal : public AActivator
{
	GENERATED_BODY()

	AActivatorPedestal();

protected:

	void BeginPlay() override;
	
	UFUNCTION()
		void OnSteppedOn(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnSteppedOff(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	void ShowButtonStateChange(bool isPressedIn);

	void SetupColorChangerMaterials();

	bool CanInteract(AActor* OtherActor);

	UPROPERTY(EditAnywhere)
		USceneComponent* Root;

	// The mesh representing the pedestal
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* Mesh;

	// The activator's trigger volume
	UPROPERTY(EditAnywhere)
		class UBoxComponent* Box;

	// The mesh representing the pedestal
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* ButtonMesh;

	UPROPERTY(EditAnywhere)
		USoundBase* PressedSound;

	UPROPERTY(EditAnywhere)
		USoundBase* ReleasedSound;

	void MoveButtonMesh(FVector NewLocation);

	UPROPERTY(EditAnywhere)
		UColorShiftingComponent* ColorShift;

public:

	TArray<class AStaticMeshActor*> GetActivatorLinks();

	UPROPERTY(EditAnywhere)
		TArray<class AStaticMeshActor*> Links;
	
};
