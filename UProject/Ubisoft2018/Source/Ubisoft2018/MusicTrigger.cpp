// Fill out your copyright notice in the Description page of Project Settings.

#include "MusicTrigger.h"
#include "DepressedCharacter.h"
#include "LightCharacter.h"
#include "SoundManager.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AMusicTrigger::AMusicTrigger()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMusicTrigger::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AMusicTrigger::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMusicTrigger::ManageBothSounds()
{
	if (SoundManager->IsCurrentSound(Sound) || SoundManager->IsCurrentSound(OptionalLoopingSound)) return;
	
	if (FadeEffect) {
		SoundManager->FadeInInitSoundAndPlayOtherSoundInLoop(Sound, OptionalLoopingSound);
	} else {
		SoundManager->PlayInitSoundAndPlayOtherSoundInLoop(Sound, OptionalLoopingSound);
	}
}

void AMusicTrigger::ManageOneSound()
{
	if (SoundManager->IsCurrentSound(Sound)) return;

	if (FadeEffect) {
		SoundManager->FadeInSound(Sound);
	} else {
		SoundManager->PlaySound(Sound);
	}
}



void AMusicTrigger::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!SoundManager || !(OtherActor->IsA(ADepressedCharacter::StaticClass()) || OtherActor->IsA(ALightCharacter::StaticClass()))) return;

	if (!HasBeenOverlapped) {
		SoundManager->SetLastActivatedTrigger(this);
		if (Sound && !OptionalLoopingSound) { 
			ManageOneSound();
		} else if (Sound && OptionalLoopingSound) { // if you are in a combat zone and you want to have an initial sound and play the looping sound after
			ManageBothSounds();
		} else {
			UE_LOG(LogTemp, Error, TEXT("Error in Music Trigger: You must select either one sound or two sounds (init and opt)."));
		}
	}

	HasBeenOverlapped = true;
}





