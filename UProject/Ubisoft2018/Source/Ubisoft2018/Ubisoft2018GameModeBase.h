// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Ubisoft2018GameModeBase.generated.h"

/**
 * 
 */

class ACharacter;
class ACheckpoint;
class UHiveMind;

UCLASS()
class UBISOFT2018_API AUbisoft2018GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

	/* Override To Read In Pawn From Custom Controller */
	UClass* GetDefaultPawnClassForController_Implementation(AController* InController) override;


public:
	AUbisoft2018GameModeBase();

	void HandleStartingNewPlayer_Implementation(APlayerController* newPlayer) override;

	void Logout(AController* controller) override;

	void RestartPlayer(AController* player) override;

	AActor* FindPlayerStart_Implementation(AController * Player, const FString & IncomingName) override;

	APawn* SpawnDefaultPawnAtTransform_Implementation(AController* NewPlayer, const FTransform& SpawnTransform) override;

	void SetCheckpoint(ACheckpoint* newCheckpoint);

	UFUNCTION()
	void HandleDeath(ACharacter* DeadCharacter, bool bKilled);

	void EndGame();

	UHiveMind * GetHiveMind();


	UPROPERTY()
	ACheckpoint* CurrentCheckpoint;

protected:
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ACharacter> DepressedCharacterClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ACharacter> LightCharacterClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UHiveMind> DarklingHiveMindClass;

	UPROPERTY()
	UHiveMind * DarklingHiveMind;

	ACheckpoint* FindStartingCheckpoint();

	void SpawnAllPlayers();
};
