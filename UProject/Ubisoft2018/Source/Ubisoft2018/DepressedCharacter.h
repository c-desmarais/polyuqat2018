// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseProjectCharacter.h"
#include "Sword.h"
#include "DepressedCharacter.generated.h"

class ADarklingCharacter;
class ABlock;
class ABlockAnchor;

UCLASS()
class UBISOFT2018_API ADepressedCharacter : public ABaseProjectCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ADepressedCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void SpawnWeapon();

	UFUNCTION(Server, Reliable, WithValidation)
	virtual void ServerEnqueueAttackRequest(FVector AttackDirection);

	UFUNCTION()
	virtual void OnResolveAttack() override;

	bool IsValidTarget(AActor* actor) override;

	UFUNCTION()
	virtual void OnStartMoving() override;

	UFUNCTION()
	virtual void OnStopMoving() override;

	UFUNCTION()
	virtual void OnFinishedAttackAnimation() override;

	UFUNCTION()
	virtual void OnFinishedHurtAnimation() override;

	void ResolveEnqueueAttack();

	void ExpireAttackRequest();

	/*Auto aim*/
	UFUNCTION()
	virtual FVector AdjustFacingVector(FVector AttackDirection) override;

	UFUNCTION()
	bool IsAutoAimPossible(FVector ActorToTarget);

	// fonctions reliees au bloc
	ABlock* FindClosestBlock();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerPushBlock();

	void DetermineInteractableBlockVisibility();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerPlaySwordSwingSound();

	UFUNCTION(NetMulticast, Reliable)
	void MulticastPlayPlaySwordSwingSound();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSetSwordDragEffectsHidden(bool Hidden);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastSetSwordDragEffectsHidden(bool Hidden);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerDissolve();

	UFUNCTION(NetMulticast, Reliable)
	void MulticastDissolve();

	UFUNCTION(NetMulticast, Reliable)
	void MulticastActivateTorchEffects(bool bIsInLightMode);

	// attributs
	UPROPERTY(EditAnywhere)
	float MaxDistanceForAutoAim = 100.0f;

	UPROPERTY(EditAnywhere)
	float MaxAngleForAutoAim = 60.0f;

	UPROPERTY(EditAnywhere)
	UParticleSystem* AttackParticleEffects;

	UPROPERTY(EditAnywhere)
	TSubclassOf<ASword> SwordClass;

	UPROPERTY(EditAnywhere)
	ASword* Sword;

	// Defines how long after an attack input is registered do we consider the request valid
	UPROPERTY(EditAnywhere)
	float AttackRequestLifespan = 0.05f;

	FVector AttackRequestDirection;

	float TimeSinceLastAttackRequest = 0.0f;

	bool bAttackEnqueued = false;

	// used when its time to push 
	ABlock* CurrentBlock = nullptr;

	ABlockAnchor* DestinationAnchor = nullptr;

	UPROPERTY(EditAnywhere)
	UAudioComponent* SwordSwingAudioComponent;

	UPROPERTY(EditAnywhere)
	USceneComponent* TorchEffects;

	UPROPERTY(EditAnywhere)
	UParticleSystemComponent* TorchBoostEffect = nullptr;

	bool bIsDying = false;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;

	virtual void Die() override;

	virtual void Jump() override;

	virtual void PushAction() override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void OnDestroyedDepressed(AActor* Act);

	// functions used by blocks (should be public)
	void CalculateClosestBlockDestinationPair(ABlock* &ClosestBlock, class ABlockAnchor* &Destination);

	void ActivateTorchEffects(bool Value);

	
};

