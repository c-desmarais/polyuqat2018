// Fill out your copyright notice in the Description page of Project Settings.

#include "BridgeSwitch.h"
#include "Bridge.h"
#include "Components/BoxComponent.h"
#include "BaseProjectCharacter.h"


// Sets default values
ABridgeSwitch::ABridgeSwitch()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	Box->SetupAttachment(RootComponent);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);

	Box->OnComponentBeginOverlap.AddDynamic(this, &ABridgeSwitch::OnOverlapBegin);
	Box->OnComponentEndOverlap.AddDynamic(this, &ABridgeSwitch::OnOverlapEnd);

}

// Called when the game starts or when spawned
void ABridgeSwitch::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABridgeSwitch::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ABridgeSwitch::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (Bridge && OtherActor->IsA(ACharacter::StaticClass())) {
		Bridge->Activate();
	}

}

void ABridgeSwitch::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (Bridge && OtherActor->IsA(ACharacter::StaticClass())) {
		Bridge->DeActivate();
	}

}