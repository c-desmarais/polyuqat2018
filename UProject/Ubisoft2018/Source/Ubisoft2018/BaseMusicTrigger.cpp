// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseMusicTrigger.h"
#include "EngineUtils.h"
#include "SoundManager.h"
#include "Components/BoxComponent.h"


// Sets default values
ABaseMusicTrigger::ABaseMusicTrigger()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	RootBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	RootComponent = RootBox;

}

// Called when the game starts or when spawned
void ABaseMusicTrigger::BeginPlay()
{
	Super::BeginPlay();
	
	
	for (TActorIterator<ASoundManager> itr(GetWorld()); itr; ++itr) {
		SoundManager = *itr;
	}

	RootBox->OnComponentBeginOverlap.AddDynamic(this, &ABaseMusicTrigger::OnOverlapBegin);
}

// Called every frame
void ABaseMusicTrigger::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABaseMusicTrigger::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//implemented in inherited classes 
}

