// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InGameWidgetsContainer.h"
#include "LevelExit.generated.h"

class UBoxComponent;

UCLASS()
class UBISOFT2018_API ALevelExit : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALevelExit();

	// Called every frame
	virtual void Tick(float DeltaTime) override;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void ListenToActivators();

	UFUNCTION()
	void OnActivated();

	UFUNCTION()
	void OnDeactivated();

	UPROPERTY(EditAnywhere)
	TArray<class AActivator*> Activators;

	UPROPERTY(EditAnywhere)
	int nbActivationsNecessary = 2;

	int nbActivations = 0;
	

	// door
	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION(NetMulticast, Reliable)
		void MulticastOpenDoor();

	UFUNCTION(NetMulticast, Reliable)
		void MulticastChangeWorld();

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* StaticMeshCabin;

	UPROPERTY(EditAnywhere)
		USkeletalMeshComponent* SkeletalMeshLeftDoor;

	UPROPERTY(EditAnywhere)
		USkeletalMeshComponent* SkeletalMeshRightDoor;

	UPROPERTY(EditAnywhere)
		USceneComponent* Root;

	UPROPERTY(EditAnywhere)
		USceneComponent* Doors;

	UPROPERTY(EditAnywhere)
		UBoxComponent* LevelEndBox;

	bool bLightCharacterPassed = false;
	bool bDepressedCharacterPassed = false;

	UPROPERTY(EditAnywhere)
		TArray<class AWorldChangerVolume*> WorldChangers;

	UPROPERTY(EditAnywhere)
		USoundBase* LevelEndSound;

	UPROPERTY(EditAnywhere)
		UForceFeedbackEffect* LevelExitForceFeedback;
};
