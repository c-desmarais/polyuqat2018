// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Wall.generated.h"

class UColorShiftingComponent;
class UAudioComponent;

UCLASS()
class UBISOFT2018_API AWall : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWall();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		TArray<class AActivator*> Activators;

	void ListenToActivators();


	UPROPERTY(EditAnywhere)
		USceneComponent* Root;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* MeshArch;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* MeshDoor;

	UPROPERTY()
		int nbOfActivatedActivators;

	UPROPERTY(EditAnywhere)
		bool IsPuzzleWall = false;

	UPROPERTY(EditAnywhere)
		UColorShiftingComponent* ColorShift;

	UPROPERTY(EditAnywhere)
		USoundBase* ActivateSound;

	UPROPERTY(EditAnywhere)
		USoundBase* DeactivateSound;

	bool IsActivated = true;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void DeactivateActivator();

	UFUNCTION(BlueprintCallable)
		void DeActivateWall();

	void ActivateWall();

	bool GetIsActivated();

	
	
};
