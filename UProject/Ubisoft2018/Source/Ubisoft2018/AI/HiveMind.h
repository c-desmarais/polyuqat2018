// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "HiveMind.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTargetLostEvent);

class UDarklingBehaviour;
class ABaseProjectCharacter;
class ACharacter;

UENUM(BlueprintType)
enum class EParameterTypes : uint8
{
	P_Dist 	UMETA(DisplayName = "DistanceBetweenPlayers"),
	P_NDark 	UMETA(DisplayName = "NumberOfDarklings")
};

USTRUCT(BlueprintType)
struct FSelectionVector 
{
	GENERATED_BODY()

	// Returns a float value of how far we are from the target values.
	float GetDistanceFromParameters(const TMap<EParameterTypes, float> & CurrentParameters);

	UPROPERTY(EditAnywhere)
		TMap<EParameterTypes, float> TargetParameters;

	UPROPERTY(EditAnywhere)
		TSubclassOf<UDarklingBehaviour> BehaviourClass;
};


/**
 * 
 */
UCLASS(Blueprintable)
class UBISOFT2018_API UHiveMind : public UObject
{
	GENERATED_BODY()

public:
	void SetGloomCharacter(ABaseProjectCharacter* GloomChar);

	void SetLumiCharacter(ABaseProjectCharacter* LumiChar);

	void IncrementProvokedDarklings();

	void DecrementProvokedDarklings();

	//Gives an AI a behaviour at random weighed according to the context
	class UDarklingBehaviour * GetRandomBehaviour();

	void OnGloomDied();

	void OnLumiDied();

	ABaseProjectCharacter * GetGloomCharacter();

	ABaseProjectCharacter * GetLumiCharacter();

	UPROPERTY()
	FTargetLostEvent OnLostGloomCharacter;

protected:

	TMap<EParameterTypes, float> GetCurrentParameters();

	UPROPERTY()
	ABaseProjectCharacter* GloomCharacter;

	UPROPERTY()
	ABaseProjectCharacter* LumiCharacter;

	UPROPERTY()
	float NbOfProvokedDarklings = 0.0f;
	
	UPROPERTY(EditDefaultsOnly)
	TArray<FSelectionVector> BehaviourSelectionWeights;
};
