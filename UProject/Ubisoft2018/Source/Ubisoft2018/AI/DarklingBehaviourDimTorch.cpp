// Fill out your copyright notice in the Description page of Project Settings.

#include "DarklingBehaviourDimTorch.h"
#include "Torch.h"
#include "DarklingAIController.h"
#include "DarklingCharacter.h"
#include "DarklingBehaviourPassive.h"
#include "VisibilityCounterComponent.h"

void UDarklingBehaviourDimTorch::SetTargetTorch(ATorch* target) {
	TargetTorch = target;
}

void UDarklingBehaviourDimTorch::OnEnter()
{
	if (TargetTorch) {
		AIController->GetDarkling()->SetStatus(EDarklingStatus::SD_Aggressive);
		AIController->MoveToActor(TargetTorch, 50.0f, true, false);
		UVisibilityCounterComponent * targetComponent = TargetTorch->FindComponentByClass<UVisibilityCounterComponent>();
		if (targetComponent) {
			targetComponent->SetCount(targetComponent->GetCount() + 1);
		}
	}
}

void UDarklingBehaviourDimTorch::OnExit()
{
	Super::OnExit();

	UVisibilityCounterComponent * targetComponent = TargetTorch->FindComponentByClass<UVisibilityCounterComponent>();
	if (targetComponent) {
		targetComponent->SetCount(targetComponent->GetCount() - 1);
	}
}

void UDarklingBehaviourDimTorch::Tick(float DeltaTime)
{
	if (!TargetTorch->GetIsLit()) {
		AIController->DecideNextBehaviour();
	}
}

void UDarklingBehaviourDimTorch::OnMoveCompleted(bool Success)
{
	if (Success) {
		TargetTorch->DimTorch();
		AIController->AttackTarget(TargetTorch);
		AIController->DecideNextBehaviour();
	}
	else {
		AIController->SetCurrentBehaviour(NewObject<UDarklingBehaviourPassive>());
	}
}
