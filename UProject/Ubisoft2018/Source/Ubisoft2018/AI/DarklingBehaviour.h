// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "DarklingBehaviour.generated.h"

class ADarklingAIController;
class ABaseProjectCharacter;

/**
 * 
 */
UCLASS(Blueprintable)
class UBISOFT2018_API UDarklingBehaviour : public UObject
{
	GENERATED_BODY()
	
public:

	void SetAIController(ADarklingAIController* AIController);

	virtual void OnEnter();

	virtual void OnExit();

	virtual void Tick(float DeltaTime);

	virtual void OnMoveCompleted(bool success);

	virtual void SetTarget(ABaseProjectCharacter* target);
	
	virtual void SetDuration(float duration);

protected:
	UFUNCTION()
		virtual void OnTargetDied(ACharacter* victim, bool killed);

	UFUNCTION()
	void FaceTargetLerp(float deltaTime);

	UFUNCTION()
	void FaceTarget();

	UFUNCTION()
	virtual void OnFaceTargetRotationCompleted();

	UPROPERTY(EditAnywhere)
	float RotationSpeed = 360.0f; // degree / s

	UPROPERTY(EditAnywhere)
	float RotationCompletedThreshold = 10.0f; // degree

	UPROPERTY()
	bool IsRotatingToFaceTarget;

	UPROPERTY()
	ADarklingAIController * AIController;

	UPROPERTY(EditAnywhere)
	// Defines how long the darkling should remain in this behaviour
	float BehaviourDuration = 2.5f;

	ABaseProjectCharacter* Target;

	UPROPERTY(EditAnywhere)
	float MoveSpeedMultiplier = 1.0f;

	float TimeSinceBehaviourStart = 0.0f;
};
