// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AI/DarklingBehaviour.h"
#include "DarklingBehaviourCautious.generated.h"

/**
 * 
 */
UCLASS()
class UBISOFT2018_API UDarklingBehaviourCautious : public UDarklingBehaviour
{
	GENERATED_BODY()

	void OnEnter() override;

	void OnExit() override;

	void Tick(float DeltaTime) override;

	void OnMoveCompleted(bool Success) override;

protected:

	UPROPERTY(EditAnywhere)
	float TargetDistance = 400.0f;

	UPROPERTY(EditAnywhere)
	float BackOffDistance = 275.0f;

	UPROPERTY(EditAnywhere)
	float CloseInDistance = 600.0f;

	bool bIsBackingOff = false;

	bool bIsClosingIn = false;
};
