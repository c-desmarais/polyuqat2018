// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DarklingBehaviour.h"
#include "DarklingBehaviourPassive.generated.h"

/**
 * 
 */
UCLASS()
class UBISOFT2018_API UDarklingBehaviourPassive : public UDarklingBehaviour
{
	GENERATED_BODY()
	
public:

	void OnEnter() override;

	void Tick(float DeltaTime) override;

	// Verifies if the given actor respects the critera for provocation
	void CheckProvocation(class ABaseProjectCharacter* provokingActor);
	
};
