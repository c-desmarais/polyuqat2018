// Fill out your copyright notice in the Description page of Project Settings.

#include "DarklingBehaviour.h"
#include "DarklingAIController.h"
#include "BaseProjectCharacter.h"
#include "DarklingCharacter.h"
#include "DarklingBehaviourPassive.h"

void UDarklingBehaviour::SetAIController(ADarklingAIController* controller)
{
	AIController = controller;
}

void UDarklingBehaviour::OnEnter()
{
	AIController->GetDarkling()->AddSpeedModifier(MoveSpeedMultiplier);
	// Override in specific behaviours
}

void UDarklingBehaviour::OnExit()
{
	AIController->GetDarkling()->RemoveSpeedModifier(MoveSpeedMultiplier);
	if (Target) {
		Target->OnDeath.RemoveDynamic(this, &UDarklingBehaviour::OnTargetDied);
	}
}

void UDarklingBehaviour::Tick(float Deltatime) {
	if ((TimeSinceBehaviourStart += Deltatime) > BehaviourDuration) {
		AIController->DecideNextBehaviour();
	}
	if (IsRotatingToFaceTarget /*&& !IsMoving*/ )
	{
		FaceTargetLerp(Deltatime);
	}
}

void UDarklingBehaviour::OnMoveCompleted(bool success)
{
	// Override in specific behaviour
}

void UDarklingBehaviour::SetTarget(ABaseProjectCharacter * target)
{
	Target = target;

	if (Target) {
		Target->OnDeath.AddDynamic(this, &UDarklingBehaviour::OnTargetDied);
	}
}

void UDarklingBehaviour::SetDuration(float duration)
{
	BehaviourDuration = duration;
}

void UDarklingBehaviour::OnTargetDied(ACharacter * victim, bool killed)
{
	AIController->SetCurrentBehaviour(NewObject<UDarklingBehaviourPassive>());
}

void UDarklingBehaviour::FaceTargetLerp(float deltaTime)
{
	ADarklingCharacter* darkling = AIController->GetDarkling();
	FVector darklingToTarget = (Target->GetActorLocation() - darkling->GetActorLocation());

	if (AIController->GetDarkling()->GetActorRotation().Equals(darklingToTarget.Rotation(), RotationCompletedThreshold))
	{
		OnFaceTargetRotationCompleted();
	}
	else
	{
		FVector darklingForward = darkling->GetActorForwardVector();
		FRotator rotation = FMath::RInterpTo(darklingForward.Rotation(), darklingToTarget.Rotation(), deltaTime, RotationSpeed);
		AIController->GetDarkling()->SetActorRotation(rotation);
	}
}

void UDarklingBehaviour::FaceTarget()
{
	if (Target && AIController)
	{
		ADarklingCharacter* darkling = AIController->GetDarkling();
		FVector darklingToTarget = (Target->GetActorLocation() - darkling->GetActorLocation());
		FRotator newRotation = FRotator(0.0f, darklingToTarget.Rotation().Yaw , 0.0f);
		darkling->SetActorRotation(newRotation);
	}
}

void UDarklingBehaviour::OnFaceTargetRotationCompleted()
{
	IsRotatingToFaceTarget = false;
}
