// Fill out your copyright notice in the Description page of Project Settings.

#include "HiveMind.h"
#include "DepressedCharacter.h"
#include "DarklingBehaviourPassive.h"

void UHiveMind::SetGloomCharacter(ABaseProjectCharacter * GloomChar)
{
	GloomCharacter = GloomChar;
}

void UHiveMind::SetLumiCharacter(ABaseProjectCharacter * LumiChar)
{
	LumiCharacter = LumiChar;
}

void UHiveMind::IncrementProvokedDarklings()
{
	NbOfProvokedDarklings += 1.0f;
}

void UHiveMind::DecrementProvokedDarklings()
{
	NbOfProvokedDarklings -= 1.0f;
}

UDarklingBehaviour * UHiveMind::GetRandomBehaviour()
{
	// if gloom is dead, default to passive behaviour
	if (!GloomCharacter) {
		return NewObject<UDarklingBehaviourPassive>();
	}

	auto currentParams = GetCurrentParameters();

	float currentTotalRatio = 0.0f;

	TArray<TPair<TSubclassOf<UDarklingBehaviour>, float>> BehaviourRatioDistribution;

	for (auto selectionVector : BehaviourSelectionWeights) {
		currentTotalRatio += 1.0f / selectionVector.GetDistanceFromParameters(currentParams);
		BehaviourRatioDistribution.Add(TPair<TSubclassOf<UDarklingBehaviour>, float>( selectionVector.BehaviourClass, currentTotalRatio ));
	}

	float distributionPoint = FMath::RandRange(0.0f, currentTotalRatio);

	//Get the behaviour associated to this point in the distribution
	for (auto BehaviourRatio : BehaviourRatioDistribution) {
		if (distributionPoint < BehaviourRatio.Value) {
			return NewObject<UDarklingBehaviour>(this, BehaviourRatio.Key);
		}
	}

	return nullptr;
}

void UHiveMind::OnGloomDied()
{
	GloomCharacter = nullptr;
	// TODO: Adjust difficulty
	if (OnLostGloomCharacter.IsBound()) {
		OnLostGloomCharacter.Broadcast();
	}
}

void UHiveMind::OnLumiDied()
{
	// TODO: Adjust difficulty
}

ABaseProjectCharacter * UHiveMind::GetGloomCharacter()
{
	return GloomCharacter;
}

ABaseProjectCharacter * UHiveMind::GetLumiCharacter()
{
	return LumiCharacter;
}

TMap<EParameterTypes, float> UHiveMind::GetCurrentParameters()
{
	TMap<EParameterTypes, float> parameters;

	// Distance between players parameter
	float distance = LumiCharacter? FVector::Dist(LumiCharacter->GetActorLocation(), GloomCharacter->GetActorLocation()) : 10000.0f;
	parameters.Add(EParameterTypes::P_Dist, distance);

	// Number of aggro darklings parameter
	parameters.Add(EParameterTypes::P_NDark, NbOfProvokedDarklings);

	return parameters;
}

float FSelectionVector::GetDistanceFromParameters(const TMap<EParameterTypes, float>& CurrentParameters)
{
	float totalDistance = 0.0f;
	for (auto parameterPair : CurrentParameters) {
		if (TargetParameters.Contains(parameterPair.Key)) {
			float param = parameterPair.Value;
			float target = TargetParameters[parameterPair.Key];
			totalDistance += FMath::Abs(target - param) / target;
		}
	}
	return totalDistance;
}
