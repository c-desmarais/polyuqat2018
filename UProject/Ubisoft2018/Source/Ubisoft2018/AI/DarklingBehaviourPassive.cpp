// Fill out your copyright notice in the Description page of Project Settings.

#include "DarklingBehaviourPassive.h"
#include "DarklingAIController.h"
#include "DarklingCharacter.h"
#include "AI/HiveMind.h"
#include "DepressedCharacter.h"

void UDarklingBehaviourPassive::OnEnter() {
	AIController->GetDarkling()->SetStatus(EDarklingStatus::SD_None);
}

void UDarklingBehaviourPassive::Tick(float DeltaTime) {
	auto GloomCharacter = AIController->GetHiveMind()->GetGloomCharacter();
	auto LumiCharacter = AIController->GetHiveMind()->GetLumiCharacter();
	CheckProvocation(GloomCharacter);
	CheckProvocation(LumiCharacter);
}

void UDarklingBehaviourPassive::CheckProvocation(ABaseProjectCharacter * provokingActor)
{
	if (provokingActor) {
		float distance = FVector::Dist(AIController->GetPawn()->GetActorLocation(), provokingActor->GetActorLocation());
		if (distance < AIController->GetDetectionRange()) {
			AIController->DecideNextBehaviour();
		}
	}
}
