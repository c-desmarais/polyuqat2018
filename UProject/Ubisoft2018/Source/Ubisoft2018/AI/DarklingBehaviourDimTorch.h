// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AI/DarklingBehaviour.h"
#include "DarklingBehaviourDimTorch.generated.h"

class ATorch;

/**
 * 
 */
UCLASS()
class UBISOFT2018_API UDarklingBehaviourDimTorch : public UDarklingBehaviour
{
	GENERATED_BODY()
	
public:

	void SetTargetTorch(ATorch* target);

	void OnEnter() override;

	void OnExit() override;

	void Tick(float DeltaTime) override;

	void OnMoveCompleted(bool Success) override;

protected:
	
	ATorch* TargetTorch;
	
};
