// Fill out your copyright notice in the Description page of Project Settings.

#include "DarklingBehaviourCautious.h"
#include "DarklingAIController.h"
#include "HiveMind.h"
#include "BaseProjectCharacter.h"
#include "DarklingCharacter.h"
#include "DarklingBehaviourAggressive.h"

void UDarklingBehaviourCautious::OnEnter()
{
	Super::OnEnter();
	AIController->GetHiveMind()->IncrementProvokedDarklings();
	AIController->GetDarkling()->SetStatus(EDarklingStatus::SD_Stalking);
	IsRotatingToFaceTarget = true;
}

void UDarklingBehaviourCautious::OnExit()
{
	Super::OnExit();
	AIController->GetHiveMind()->DecrementProvokedDarklings();
}

void UDarklingBehaviourCautious::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!Target || !AIController->GetDarkling()) {
		return;
	}

	// Get the vector from target to us.
	FVector targetToDarkling = AIController->GetDarkling()->GetActorLocation() - Target->GetActorLocation();

	// if too close, back off directly
	if (targetToDarkling.Size() < BackOffDistance) {
		FVector destination = Target->GetActorLocation() + targetToDarkling.GetSafeNormal2D()*TargetDistance;
		AIController->MoveToLocation(destination, 10.0f);
	}
	// if too far, close in
	else if(targetToDarkling.Size() > CloseInDistance){
		AIController->MoveToActor(Target, TargetDistance);
	}
}

void UDarklingBehaviourCautious::OnMoveCompleted(bool Success)
{
	IsRotatingToFaceTarget = true;
}