#include "DarklingBehaviourAggressive.h"
#include "DarklingCharacter.h"
#include "DarklingAIController.h"
#include "VisibilityCounterComponent.h"
#include "AI/HiveMind.h"

void UDarklingBehaviourAggressive::OnEnter()
{
	Super::OnEnter();

	AIController->GetHiveMind()->IncrementProvokedDarklings();

	if (Target) {
		UVisibilityCounterComponent * targetComponent = Target->FindComponentByClass<UVisibilityCounterComponent>();
		if (targetComponent) {
			targetComponent->SetCount(targetComponent->GetCount() + 1);
		}
	}

	// Get attack range
	AttackRange = AIController->GetDarkling()->GetAttackRange();

	AIController->GetDarkling()->SetStatus(EDarklingStatus::SD_Aggressive);

	// Start Aggro anim
	FaceTarget();
	AIController->GetDarkling()->StartAggroAnim();
	bool bIsPlayingAggroAnim = true;
}

void UDarklingBehaviourAggressive::OnExit()
{
	Super::OnExit();

	AIController->GetHiveMind()->DecrementProvokedDarklings();

	if (Target) {
		UVisibilityCounterComponent * targetComponent = Target->FindComponentByClass<UVisibilityCounterComponent>();
		if (targetComponent) {
			targetComponent->SetCount(targetComponent->GetCount() - 1);
		}
	}
}

void UDarklingBehaviourAggressive::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsPlayingAggroAnim) {
		if (TimeSinceBehaviourStart >= TimeUntilMovementBegins) {
			OnAggroAnimFinished();
		}
		return;
	}

	if (bIsWaitingForReachableTarget ) {
		if ((TimeSinceLastPathCheck += DeltaTime) >= TimeBetweenPathChecks) {
			StartMovingToTarget();
		}
	}
	else if (!bIsOnTheMove && Target) {
		float distance = FVector::Dist( Target->GetActorLocation(), AIController->GetDarkling()->GetActorLocation());
		if (distance < AttackRange) {
			if ((TimeUntilNextAttack -= DeltaTime) <= 0) {
				PerformAttack();
			}
		}
		else {
			StartMovingToTarget();
		}
	}
}

void UDarklingBehaviourAggressive::OnMoveCompleted(bool Success)
{
	if (!Success) {
		bIsWaitingForReachableTarget = true;
	}
	bIsOnTheMove = false;
	TimeUntilNextAttack = TimeUntilAttackAfterMove;
}

void UDarklingBehaviourAggressive::PerformAttack()
{
	// Get attack vector
	AIController->AttackTarget(Target);
	TimeUntilNextAttack = TimeBetweenAttacks;
}

void UDarklingBehaviourAggressive::OnAggroAnimFinished()
{
	bIsPlayingAggroAnim = false;
	StartMovingToTarget();
}

void UDarklingBehaviourAggressive::StartMovingToTarget()
{
	bIsOnTheMove = true;
	AIController->MoveToActor(Target, AttackRange / 2);
}
