// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DarklingBehaviour.h"
#include "DarklingBehaviourAggressive.generated.h"


class ACharacter;
/**
 * 
 */
UCLASS()
class UBISOFT2018_API UDarklingBehaviourAggressive : public UDarklingBehaviour
{
	GENERATED_BODY()
	
public:

	void OnEnter() override;

	void OnExit() override;

	void Tick(float DeltaTime) override;

	void OnMoveCompleted(bool Success) override;

protected:

	void PerformAttack();

	UFUNCTION()
	void OnAggroAnimFinished();

	void StartMovingToTarget();

	UPROPERTY(EditAnywhere)
	float TimeBetweenAttacks = 0.6f;

	UPROPERTY(EditAnywhere)
	float TimeUntilAttackAfterMove = 0.5f;

	UPROPERTY(EditAnywhere)
	float TimeUntilMovementBegins = 1.0f;

	float TimeUntilNextAttack = 0.0f;

	float TimeBetweenPathChecks = 1.0f;

	float TimeSinceLastPathCheck = 0.0f;

	float AttackRange = 0.0f;

	bool bIsOnTheMove = false;

	bool bIsWaitingForReachableTarget = false;

	bool bIsPlayingAggroAnim = true;
};
