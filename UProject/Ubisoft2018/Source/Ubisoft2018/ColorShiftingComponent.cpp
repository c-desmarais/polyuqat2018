// Fill out your copyright notice in the Description page of Project Settings.

#include "ColorShiftingComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Engine.h"


// Sets default values for this component's properties
UColorShiftingComponent::UColorShiftingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	bIsInLightMode = false;
	LightAmount = 0.0f;		// Value of 1 means is Lit
}


// Called when the game starts
void UColorShiftingComponent::BeginPlay()
{
	Super::BeginPlay();	
}


// Called every frame
void UColorShiftingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (bIsInterpolating) {
		LightAmount += bIsInLightMode ? DeltaTime : -DeltaTime;
		LightAmount = FMath::Clamp(LightAmount, 0.0f, 1.0f);

		for (UMaterialInstanceDynamic* material : DynamicMaterials) {
				material->SetScalarParameterValue(FName("LightAmount"), LightAmount);
		}

		if (LightAmount == 0.0f || LightAmount == 1.0f) {
			bIsInterpolating = false;
		}
	}
}


void UColorShiftingComponent::ServerSetIsInLightMode_Implementation(bool Value)
{
	MulticastSetIsInLightMode(Value);
}

bool UColorShiftingComponent::ServerSetIsInLightMode_Validate(bool Value)
{
	return true;
}

void UColorShiftingComponent::MulticastSetIsInLightMode_Implementation(bool Value)
{
	SetIsInLightMode(Value);
}

void UColorShiftingComponent::SetIsInLightMode(bool Value)
{
	if (bIsInLightMode != Value) {
		bIsInLightMode = Value;
		bIsInterpolating = true;
		if (!bCreatedDynamicMaterials) {
			MakeAllTargetMaterialsDynamic();
			bCreatedDynamicMaterials = true;
		}
	}
}

void UColorShiftingComponent::SetLightAmount(float Value)
{
	if (Value > 1.0f) {
		Value = 1.0f;
	}
	else if (Value < 0.0f) {
		Value = 0.0f;
	}

	LightAmount = Value;
}

bool UColorShiftingComponent::GetIsInLightMode()
{
	return bIsInLightMode;
}

void UColorShiftingComponent::AddTargetMeshMaterial(UStaticMeshComponent* Mesh, int MaterialIndex)
{
	FMeshMaterialIndexPair newTarget;
	newTarget.Mesh = Mesh;
	newTarget.MaterialIndex = MaterialIndex;
	TargetMaterials.Add(newTarget);
	if (bCreatedDynamicMaterials) {
		SetupDynamicMaterial(Mesh, MaterialIndex);
	}
}

void UColorShiftingComponent::MakeAllTargetMaterialsDynamic()
{
	for (auto MeshMatIndexPair : TargetMaterials) {
		SetupDynamicMaterial(MeshMatIndexPair.Mesh, MeshMatIndexPair.MaterialIndex);
	}
}

void UColorShiftingComponent::SetupDynamicMaterial(UStaticMeshComponent * targetMesh, int targetMaterialIndex)
{
	if (targetMesh && targetMesh->GetMaterial(targetMaterialIndex)) {
		DynamicMaterials.Add(targetMesh->CreateDynamicMaterialInstance(targetMaterialIndex));
	}
}

