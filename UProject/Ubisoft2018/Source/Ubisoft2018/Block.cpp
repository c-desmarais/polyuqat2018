// Fill out your copyright notice in the Description page of Project Settings.

#include "Block.h"
#include "Components/BoxComponent.h"
#include "DepressedCharacter.h"
#include "LightCharacter.h"
#include "DarklingCharacter.h"
#include "BlockAnchor.h"
#include "UnrealNetwork.h"
#include "ActivatorPedestal.h"
#include "ColorShiftingComponent.h"
#include "Wall.h"

// Sets default values
ABlock::ABlock()
{
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	RootComponent = Mesh;

	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	Box->SetupAttachment(RootComponent);

	DepressedNearbyEffects = CreateDefaultSubobject<USceneComponent>(TEXT("DepressedNearbyEffects"));
	DepressedNearbyEffects->SetupAttachment(RootComponent);
	
	ColorShift = CreateDefaultSubobject<UColorShiftingComponent>(TEXT("ColorShift"));

	SoundEffects = CreateDefaultSubobject<USceneComponent>(TEXT("SoundEffects"));
	SoundEffects->SetupAttachment(RootComponent);

	InputAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("InputAudioComponent"));
	InputAudioComponent->SetupAttachment(SoundEffects);

	MovementAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("MovementAudioComponent"));
	MovementAudioComponent->SetupAttachment(SoundEffects);

	bReplicates = true;
	bReplicateMovement = true;
}

// Called when the game starts or when spawned
void ABlock::BeginPlay()
{
	Super::BeginPlay();
	Mesh->OnComponentHit.AddDynamic(this, &ABlock::OnOverlapBegin);
	ColorShift->AddTargetMeshMaterial(Mesh, 0);
	DepressedNearbyEffects->SetHiddenInGame(true, true);
	BlockRadius = Box->GetScaledBoxExtent().X / 2 + BlockExtraRadius; 
}

// Called every frame
void ABlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!HasAuthority()) return;
	
	if (IsMoving && DestinationAnchor) {
		ProcessMovement(DeltaTime);
	}
}

void ABlock::ProcessMovement(float deltaTime) {

	float stepSize = SpeedMultiplier * deltaTime;
	FVector currentLocation = GetActorLocation();
	FVector destinationLocation = DestinationAnchor->GetActorLocation();

	bool reachedDestination = stepSize >= FVector::Dist(currentLocation, destinationLocation);

	FVector newPosition = reachedDestination ? destinationLocation : GetActorLocation() + stepSize*MoveDirection ;

	SetActorLocation(newPosition, true);
	
	if (reachedDestination) {
		StopMovement();
		InitializeTrajectory(DestinationAnchor);
	}
}

void ABlock::OnOverlapBegin(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!HasAuthority()) return;
	
	if (IsMoving) {
		ABaseProjectCharacter* baseCharacter = Cast<ABaseProjectCharacter>(OtherActor);
		if (baseCharacter && IsActorBlockingTheWayToDestination(baseCharacter, CharacterBlockingAngleThreshold)) {
			baseCharacter->Die();
		}
		else if ((OtherActor->IsA(ABlock::StaticClass()) || OtherActor->IsA(AWall::StaticClass())) 
			&& IsActorBlockingTheWayToDestination(OtherActor, WorldBlockingAngleThreshold)) {
			MulticastRPCPutBlockOnHold();
		}
	}
}

void ABlock::MoveBlockToDestination(ABlockAnchor* blockAnchor) {
	IsMoving = true;
	InATrajectory = true;
	SourcePosition = GetActorLocation();
	SourceAnchor = CurrentBlockAnchor;
	DestinationAnchor = blockAnchor;
	MoveDirection = (DestinationAnchor->GetActorLocation() - SourcePosition).GetSafeNormal();
	MulticastStartMove();
}

void ABlock::MulticastStartMove_Implementation()
{
	InputAudioComponent->SetSound(PushSound);
	InputAudioComponent->Play();
	MovementAudioComponent->Play();
	ColorShift->SetIsInLightMode(true);
}

ABlockAnchor* ABlock::IsBlockOnAnchorPoint() {
	ABlockAnchor* overlappingAnchorPoint = nullptr;
	TArray<AActor*> OverlappingActors;
	GetOverlappingActors(OverlappingActors);
	float minDist = minDistanceBtwBlockAndUnderAnchorPoint;
	for (AActor* actor : OverlappingActors) {
		ABlockAnchor* anchor = Cast<ABlockAnchor>(actor);
		if (anchor && anchor != DestinationAnchor) {
			float distance = FVector::Dist(anchor->GetActorLocation(), GetActorLocation());
			if (distance < minDist) {
				minDist = distance;
				overlappingAnchorPoint = anchor;
				SetActorLocation(anchor->GetActorLocation());
			}
		}

	}
	return overlappingAnchorPoint;
}

void ABlock::InitializeTrajectory(ABlockAnchor* currentAnchorPoint) {
	CurrentBlockAnchor = currentAnchorPoint;
	DestinationAnchor = nullptr;
	SourceAnchor = nullptr;
	InATrajectory = false;
}

void ABlock::StopMovement() {
	IsMoving = false;
	MulticastStopMovement();
}

void ABlock::MulticastStopMovement_Implementation()
{
	ColorShift->SetIsInLightMode(false);
	InputAudioComponent->SetSound(LandSound);
	InputAudioComponent->Play();
	MovementAudioComponent->Stop();
}

void ABlock::MulticastRPCPutBlockOnHold_Implementation() {
	StopMovement();
	ABlockAnchor* anchorPoint = IsBlockOnAnchorPoint();
	if (anchorPoint) {
		InitializeTrajectory(anchorPoint);
	}
}

TArray<AActor*> ABlock::GetBlockingActors() {
	TArray<AActor*> OverlappingActors;
	TArray<AActor*> BlockingActors;
	GetOverlappingActors(OverlappingActors);
	for (AActor* actor : OverlappingActors) {
		AWall* wall = Cast<AWall>(actor);
		
		if (actor->IsA(ABlock::StaticClass()) ||
			(wall && wall->GetIsActivated())) { 
			BlockingActors.Add(actor);
		}
	}
	return BlockingActors;
}

bool ABlock::PushingTowardsBlockOrWall(FVector depressedPosition) {
	FVector vectDepressedBlock = depressedPosition - GetActorLocation();
	FVector vectBlockingActBlock;
	for (AActor* actor : GetBlockingActors()) {
		vectBlockingActBlock = actor->GetActorLocation() - GetActorLocation();
		// keep the absolute value here, gives you the right angle
		float angle = FMath::RadiansToDegrees(FMath::Acos(FMath::Abs(vectDepressedBlock.CosineAngle2D(vectBlockingActBlock))));
		if (angle < 45.f) { 
			return true;
		}
	}
	return false;
}


bool ABlock::IsActorBlockingTheWayToDestination(AActor* actor, float angleThreshold) {
	FVector actorLocation = actor->GetActorLocation();
	FVector blockLocation = GetActorLocation();
	FVector destinationLocation;
	if (DestinationAnchor) {
		destinationLocation = DestinationAnchor->GetActorLocation(); // this will not be nullptr
	}
	
	FVector vectorA = actorLocation - blockLocation;
	FVector vectorB = destinationLocation - blockLocation;

	float angle = FMath::Abs(FMath::RadiansToDegrees(FMath::Acos(vectorA.CosineAngle2D(vectorB)))); // do not apply val abs (angle is correct that way)
	return angle <= angleThreshold; // since 45 is diagonal (it is possible that it gets there around 44.8 or a biti lower, you dont want it to block here)
}

ABlockAnchor* ABlock::FindDestinationBlockAnchor(FVector depressedPosition, FVector blockPosition, TArray<ABlockAnchor*> possibleDestinations) {
	FVector vectorA = blockPosition - depressedPosition;
	float magnitudeVectorA = FMath::Sqrt(FMath::Pow(vectorA.X, 2) + FMath::Pow(vectorA.Y, 2) + FMath::Pow(vectorA.Z, 2)); //norme = magnitude in english
	float minAngle = 10000.f;
	ABlockAnchor* closestBlockAnchor = nullptr;
	// calculer la distance entre point dancrage courant du bloc tous les points voisins 

	for (ABlockAnchor* anchor : possibleDestinations) {
		if (anchor) {
			FVector vectorB = blockPosition - anchor->GetActorLocation();
			FVector end = anchor->GetActorLocation();

			float angle = FMath::RadiansToDegrees(FMath::Acos(FMath::Abs(vectorA.CosineAngle2D(vectorB))));
			float angleWithoutValAbs = FMath::RadiansToDegrees(FMath::Acos(vectorA.CosineAngle2D(vectorB)));
			float roundedAngle = int(angle * FMath::Pow(10, 3)) / (FMath::Pow(10, 3));
			float roundedMinAngle = int(minAngle * FMath::Pow(10, 3)) / (FMath::Pow(10, 3));

			if ((roundedAngle <= roundedMinAngle) && (!FMath::IsNearlyEqual(angle, angleWithoutValAbs)) && (angle <= 45.0f)) {
				minAngle = angle;
				closestBlockAnchor = anchor;
			}
		}
	}
	return closestBlockAnchor;
}

ABlockAnchor* ABlock::CalculateDestinationBlockAnchor(FVector depressedPosition) {
	// si il est sur le hold, tu na pas le droit de pousser dans la direction de lacteur bloquant
	if (PushingTowardsBlockOrWall(depressedPosition)) {
		return nullptr; // et que tu pousse vers un acteur
	}

	if (InATrajectory && !IsMoving) { //si le block est bloque 
		TArray<ABlockAnchor*> possibleDestinations;
		possibleDestinations.Add(SourceAnchor);
		possibleDestinations.Add(DestinationAnchor);
		FVector currentPosition = GetActorLocation();
		return FindDestinationBlockAnchor(depressedPosition, GetActorLocation(), possibleDestinations);
	}
	
	return FindDestinationBlockAnchor(depressedPosition, CurrentBlockAnchor->GetActorLocation(), CurrentBlockAnchor->GetNeighbours());
}

void ABlock::SetCurrentBlockAnchor(ABlockAnchor* blockAnchor) {
	CurrentBlockAnchor = blockAnchor;
}

void ABlock::ClientDisplayCanInterract_Implementation(bool IsDisplayed, ADepressedCharacter* depressed) {
	DepressedNearbyEffects->SetHiddenInGame(!IsDisplayed, true);
}

bool ABlock::IsPositionWithinBlockRadius(FVector position) {
	FVector distanceVector = position - GetActorLocation();
	distanceVector.Z = 0;
	return distanceVector.Size() <= BlockRadius;
}






