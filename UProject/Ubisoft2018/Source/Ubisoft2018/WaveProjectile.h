// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "WaveProjectile.generated.h"

class USphereComponent;


UCLASS()
class UBISOFT2018_API AWaveProjectile : public AActor
{
	GENERATED_BODY()
	AWaveProjectile(const FObjectInitializer& ObjectInitializer);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
	USphereComponent* CollisionComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement)
	UProjectileMovementComponent* ProjectileMovement;

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	UFUNCTION(NetMulticast, Reliable)
	void MulticastPlayHitEffects(FVector Location);

	UPROPERTY(EditAnywhere)
	UParticleSystem* HitParticleEffects;

	UPROPERTY(EditAnywhere)
		float StunTime = 2.f;

	UPROPERTY(EditAnywhere, Category = "WaveProjectile|SFX")
	USoundBase* SpawnSound;

	UPROPERTY(EditAnywhere, Category = "WaveProjectile|SFX")
	USoundBase* HitSound;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// This launches the projectile
	void Launch(const FVector& ShootDirection);
	
};
