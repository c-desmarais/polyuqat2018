// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Bridge.generated.h"

class UAudioComponent;
class USoundBase;

UCLASS()
class UBISOFT2018_API ABridge : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABridge();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void Activate();

	UFUNCTION(BlueprintCallable)
	void DeActivate();

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere)
	USceneComponent* Root;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	TArray<class AActivator*> Activators;

	void ListenToActivators();

	void ShowBridge();

	void HideBridge();

	UPROPERTY()
	class UMaterialInstanceDynamic* DynamicMaterial;

	float MaterialAmount;

	bool bIsInterpolating;

	UPROPERTY()
		USceneComponent* SoundEffects;

	UPROPERTY(EditAnywhere)
		UAudioComponent* OnOffAudioComponent;

	UPROPERTY(EditAnywhere)
		UAudioComponent* AmbiantAudioComponent;

	UPROPERTY(EditAnywhere)
		USoundBase* OnActivateSound;

	UPROPERTY(EditAnywhere)
		USoundBase* OnDeactivateSound;

	bool bIsFadingIn = false;

private: 
	UPROPERTY()
	int NbSwitchesActivated;
	
};
