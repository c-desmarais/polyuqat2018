// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseProjectCharacter.h"
#include "DarklingCharacter.generated.h"

class UMaterialBillboardComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FStunEvent, float, StunTime);

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EDarklingStatus : uint8
{
	SD_Aggressive 	UMETA(DisplayName = "Aggressive"),
	SD_Stunned 		UMETA(DisplayName = "Stunned"),
	SD_Stalking 	UMETA(DisplayName = "Stalking"),
	SD_None			UMETA(DisplayName = "None")
};

/**
 * 
 */
UCLASS()
class UBISOFT2018_API ADarklingCharacter : public ABaseProjectCharacter
{
	GENERATED_BODY()
	
public:
	ADarklingCharacter();

	virtual void Die() override;

	void SetStatus(EDarklingStatus status);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastSetStatus(EDarklingStatus status);

	float GetAttackRange();

	void Stun(float DeltaTime);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastSetTorchEffects(bool isLit);

	void SetupAnimInstance() override;

	void StartAggroAnim();

	UFUNCTION()
	void OnFinishedAggroAnimation();

	FStunEvent OnStunned;

protected:

	UFUNCTION(NetMulticast, Reliable)
	void MulticastPlayAttackParticle();

	void BeginPlay() override;

	UFUNCTION()
	virtual void OnResolveAttack() override;

	// Checks if the target actor can be attacked at the moment
	bool IsValidTarget(AActor* actor) override;

	// Spawns the AI controller for this Darkling if it doesn<T have one already
	void SpawnController();
	
	// Actually applies the damage from the darkling's attack
	void ResolveDarklingAttack();

	UPROPERTY(EditAnywhere)
	UParticleSystem* AttackParticleEffects;

	UParticleSystemComponent* SmokePS = nullptr;

	UPROPERTY(EditAnywhere)
	USceneComponent* TorchEffects;

	UPROPERTY(EditAnywhere)
	UParticleSystemComponent* TorchSlowEffect = nullptr;



private:

	UPROPERTY(EditDefaultsOnly)
		UMaterialBillboardComponent* StatusBillboard;

	UPROPERTY(EditDefaultsOnly)
		TMap<EDarklingStatus, UMaterialInterface*> StatusMaterials;

};
