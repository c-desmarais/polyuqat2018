// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BlockAnchor.generated.h"

UCLASS()
class UBISOFT2018_API ABlockAnchor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABlockAnchor();

	TArray<ABlockAnchor*> GetNeighbours();

	void SetNeighbours(TArray<ABlockAnchor*> neighbours);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void SpawnBlock();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
	class UStaticMeshComponent* PreviewMesh;

	// to change eventually : this is public for now because i am not sure how unreal deals with private and edit anywhere when together
	UPROPERTY(EditAnywhere)
	TArray<ABlockAnchor*> BlockAnchorNeighbours;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class ABlock> BlockClass;

	UPROPERTY(EditAnywhere)
	bool bSpawnBlock;
	
};
