// Fill out your copyright notice in the Description page of Project Settings.

#include "DepressedCharacter.h"
#include "EngineUtils.h"
#include "Block.h"
#include "BlockAnchor.h"
#include "DrawDebugHelpers.h"
#include "Components/SkeletalMeshComponent.h"
#include "UbisoftAnimInstance.h"
#include "DarklingCharacter.h"
#include "MonsterSpawner.h"
#include "CustomUtils.h"
#include "Materials/MaterialInstanceDynamic.h"


// Sets default values
ADepressedCharacter::ADepressedCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SwordSwingAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("SwordSwingAudioComponent"));
	SwordSwingAudioComponent->SetupAttachment(SoundEffects);

	TorchEffects = CreateDefaultSubobject<USceneComponent>(TEXT("TorchEffects"));
	TorchEffects->SetupAttachment(RootComponent);

	TorchBoostEffect = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("TorchBoostEffect"));
	TorchBoostEffect->SetupAttachment(TorchEffects);
	TorchBoostEffect->SetRelativeLocation(FVector::ZeroVector);

	OnDestroyed.AddDynamic(this, &ADepressedCharacter::OnDestroyedDepressed);
}

// Called when the game starts or when spawned
void ADepressedCharacter::BeginPlay()
{
	Super::BeginPlay();
	bUseControllerRotationYaw = false;

	MulticastActivateTorchEffects(IsInLightModeWorldChanger());

	TorchEffects->SetVisibility(false, true);
	SpawnWeapon();
}

// Called every frame
void ADepressedCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bAttackEnqueued && ((TimeSinceLastAttackRequest += DeltaTime) > AttackRequestLifespan)) {
		ExpireAttackRequest();
	}

	// Cla : Ive talked about this call with Camille to validate system. Its not that time consuming.
	DetermineInteractableBlockVisibility();

	if (bIsDying && !bIsSpawning) {
		DissolveAmount += DeltaTime * DissolveRateMultiplier;
		DissolveAmount = FMath::Clamp(DissolveAmount, -1.0f, 6.0f);

		for (UMaterialInstanceDynamic* material : DynamicMaterials) {
			material->SetScalarParameterValue(FName("Disolve"), DissolveAmount);
		}

		if (DissolveAmount == 6.0f) {
			bIsDying = false;
		}
	}
}

float ADepressedCharacter::TakeDamage(float DamageAmount, FDamageEvent const & DamageEvent, AController * EventInstigator, AActor * DamageCauser)
{
	// The player is immune to damage when he's already taking damage
	if (CharacterAnimParameters.bIsTakingDamage == true)
	{
		return 0.0f;
	}
	else
	{
		return Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	}
}

void ADepressedCharacter::Die()
{
	CharacterAnimParameters.bIsDying = true;
	ReplicateAnimParameters();

	if (HasAuthority())
	{
		ServerDissolve();
		ServerPlayDeathParticle();
		ServerPlayDeathSound();
	}
}

void ADepressedCharacter::SpawnWeapon()
{
	if (!SwordClass) {
		return;
	}
	FActorSpawnParameters ActorSpawnParameters;
	Sword = GetWorld()->SpawnActor<ASword>(SwordClass, ActorSpawnParameters);

	// Attach the sword
	FName NameWeaponSocket = TEXT("WeaponSocket");
	Sword->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, NameWeaponSocket);

}

void ADepressedCharacter::ServerEnqueueAttackRequest_Implementation(FVector AttackDirection)
{
	if (CanAttack()) {
		Super::ServerEnqueueAttackRequest_Implementation(AttackDirection);
		ServerPlaySwordSwingSound();
	}
	else {
		// Enqueue the attack request
		bAttackEnqueued = true;
		TimeSinceLastAttackRequest = 0;
		AttackRequestDirection = AttackDirection;
	}
}

bool ADepressedCharacter::ServerEnqueueAttackRequest_Validate(FVector AttackDirection)
{
	return true;
}

void ADepressedCharacter::OnResolveAttack()
{
	Super::OnResolveAttack();
	if (HasAuthority()) {
		for (TActorIterator<AMonsterSpawner> itr(GetWorld()); itr; ++itr) {
			// Extend range against monster spawners due to radius
			if (IsValidTarget(*itr)) {
				FDamageEvent damageEvent(UDamageType::StaticClass());
				itr->TakeDamage(AttackDamage, damageEvent, GetController(), this);
			}
		}
	}
}

bool ADepressedCharacter::IsValidTarget(AActor* actor) {
	bool canBeHurt = actor && (actor->IsA(AMonsterSpawner::StaticClass()) || actor->IsA(ADarklingCharacter::StaticClass()));

	return canBeHurt && Super::IsValidTarget(actor);
}

void ADepressedCharacter::OnStartMoving()
{
	ServerSetSwordDragEffectsHidden(false);
}

void ADepressedCharacter::OnStopMoving()
{
	ServerSetSwordDragEffectsHidden(true);
}

void ADepressedCharacter::OnFinishedAttackAnimation()
{
	ResolveEnqueueAttack();
}

void ADepressedCharacter::OnFinishedHurtAnimation()
{
	Super::OnFinishedHurtAnimation();
	ResolveEnqueueAttack();
}

void ADepressedCharacter::ResolveEnqueueAttack()
{
	if (HasAuthority()) {
		if (bAttackEnqueued) {
			CharacterAnimParameters.bIsAttacking = true;
			ReplicateAnimParameters(AdjustFacingVector(AttackRequestDirection));
			MulticastPlayPlaySwordSwingSound();
		}
		else {
			CharacterAnimParameters.bIsAttacking = false;
			ReplicateAnimParameters();
		}
	}
}

void ADepressedCharacter::ExpireAttackRequest()
{
	bAttackEnqueued = false;
}

// Called to bind functionality to input
void ADepressedCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ADepressedCharacter::Jump() {
	// Do nothing
}

void ADepressedCharacter::PushAction()
{
	ServerPushBlock();
}

void ADepressedCharacter::ServerPushBlock_Implementation()
{
	// this info is set in the block directly, everytime a depressed is near a block.
	if (CurrentBlock && DestinationAnchor) {
		CurrentBlock->MoveBlockToDestination(DestinationAnchor);
	}
}

bool ADepressedCharacter::ServerPushBlock_Validate()
{
	return true;
}

void ADepressedCharacter::CalculateClosestBlockDestinationPair(ABlock* &ClosestBlock, ABlockAnchor* &Destination)
{
	ClosestBlock = FindClosestBlock();
	if (ClosestBlock) {
		Destination = ClosestBlock->CalculateDestinationBlockAnchor(GetActorLocation());
	}
}

void ADepressedCharacter::DetermineInteractableBlockVisibility() {
	if (!HasAuthority()) return;
	ABlock* block;
	ABlockAnchor* destination;
	CalculateClosestBlockDestinationPair(block, destination);

	if (block != CurrentBlock) { // if block has changed
		if (CurrentBlock && IsLocallyControlled()) { // depressed is locally controlled
			CurrentBlock->ClientDisplayCanInterract(false, this); 
		}
		CurrentBlock = block;
	}
	if (destination != DestinationAnchor) { // if destination has changed
		if (block) {
			block->ClientDisplayCanInterract(destination != nullptr, this);
		}
		DestinationAnchor = destination;
	}
}




void ADepressedCharacter::OnDestroyedDepressed(AActor * Act) {
	if (Sword != nullptr) {
		GetWorld()->DestroyActor(Sword);
		Sword = nullptr;
	}
}

ABlock* ADepressedCharacter::FindClosestBlock() {
	ABlock* closestBlock = UCustomUtils::GetNearestActorOfType<ABlock>(GetActorLocation(), GetWorld());

	if (!closestBlock) {
		return nullptr;
	}

	if (closestBlock->IsPositionWithinBlockRadius(GetActorLocation())) {
		return closestBlock;
	}
	else {
		return nullptr;
	}
}

FVector ADepressedCharacter::AdjustFacingVector(FVector AttackDirection) {
	float distNearestTarget = MaxDistanceForAutoAim;
	for (TActorIterator<ADarklingCharacter> iterator(GetWorld()); iterator; ++iterator) {
		// Check if we are in range and in angle for an auto aim attack
		FVector actorToTarget = iterator->GetActorLocation() - GetActorLocation();
		if (IsAutoAimPossible(actorToTarget) && actorToTarget.Size() < distNearestTarget) {
			AttackDirection = actorToTarget;
			distNearestTarget = actorToTarget.Size();
		}
	}
	return Super::AdjustFacingVector(AttackDirection);
}

bool ADepressedCharacter::IsAutoAimPossible(FVector ActorToTarget) {
	// Check if Depressed points toward the target and is near enough
	if (ActorToTarget.Size() < MaxDistanceForAutoAim) {
		FVector normalDepressedToTarget = ActorToTarget.GetSafeNormal();
		FVector normalForward = GetActorForwardVector().GetSafeNormal();
		return FVector::Coincident(normalDepressedToTarget, normalForward, cosf(FMath::DegreesToRadians(MaxAngleForAutoAim)));
	}
	return false;
}

bool ADepressedCharacter::ServerPlaySwordSwingSound_Validate()
{
	return true;
}

void ADepressedCharacter::ServerPlaySwordSwingSound_Implementation()
{
	MulticastPlayPlaySwordSwingSound();
}

void ADepressedCharacter::MulticastPlayPlaySwordSwingSound_Implementation()
{
	if (SwordSwingAudioComponent) {
		SwordSwingAudioComponent->Play();
	}
}

void ADepressedCharacter::ActivateTorchEffects(bool Value)
{
	if (TorchEffects) {
		TorchEffects->SetVisibility(Value, true);
	}
}

bool ADepressedCharacter::ServerSetSwordDragEffectsHidden_Validate(bool Hidden)
{
	return true;
}

void ADepressedCharacter::ServerSetSwordDragEffectsHidden_Implementation(bool Hidden)
{
	MulticastSetSwordDragEffectsHidden(Hidden);
}

void ADepressedCharacter::MulticastSetSwordDragEffectsHidden_Implementation(bool Hidden)
{
	Sword->SetSwordDragEffectsHidden(Hidden);
}

bool ADepressedCharacter::ServerDissolve_Validate() {
	return true;
}

void ADepressedCharacter::ServerDissolve_Implementation() {
	MulticastDissolve();
}

void ADepressedCharacter::MulticastDissolve_Implementation() 
{
	bIsDying = true;
}

void ADepressedCharacter::MulticastActivateTorchEffects_Implementation(bool bIsInLightMode)
{
	ActivateTorchEffects(bIsInLightMode);
}
