// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LevelCamera.generated.h"

UCLASS()
class UBISOFT2018_API ALevelCamera : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALevelCamera();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SetPrimaryController(AController* controller);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
private:
	FVector GetTargetsCenterOfMass();

	// Adjusts the camera's height accordint to tracking parameters
	float ComputeTargetArmLength(const FVector & centerOfMass);

	float GetLargestArmLengthForActor(AActor* actor, const FVector & centerOfMass);

	void InitializePerspectiveTrackingData();

	UPROPERTY(EditAnywhere)
	class UCameraComponent* CameraComponent;

	UPROPERTY(EditAnywhere)
	class USpringArmComponent* SpringArmComponent;

	UPROPERTY(EditAnywhere, Category = "LevelCamera|PerspectiveMode")
	float MinimumArmLength = 500.0f;

	UPROPERTY(EditAnywhere, Category = "LevelCamera|PerspectiveMode")
	float MaximumArmLength = 1500.0f;

	UPROPERTY(EditAnywhere, Category = "LevelCamera|PerspectiveMode")
	float MaximumZDifference = 400.0f;

	UPROPERTY(EditAnywhere, Category = "LevelCamera|PerspectiveMode")
	float ArmLengthSpeed = 1000.0f;


	// furthest screen distance a target can be away from in horizontal space from center
	UPROPERTY(EditAnywhere, Meta = (ClampMin = "0.0", ClampMax = "1.0"))
	float MaxHorNormalizedDistanceFromCenter = 0.45;

	// furthest screen distance a target can be away from in vertical space from center
	UPROPERTY(EditAnywhere, Meta = (ClampMin = "0.0", ClampMax = "1.0"))
	float MaxVertNormalizedDistanceFromCenter = 0.45;

	// The furthest yaw a character can be from the camera's forward vector
	UPROPERTY()
	float TargetMaximumYawTan = 0.0f;

	// The furthest pitch a character can be from the camera's forward vector
	UPROPERTY()
	float TargetMaximumPitchTan = 0.0f;

	UPROPERTY()
	float currentZDifference = 0.0f;

	UPROPERTY()
		AController* PrimaryController;

	UPROPERTY(EditAnywhere)
	float DistancedCameraVignette = 1.0f;

	UPROPERTY(EditAnywhere)
	float NearCameraVignette = 0.5f;

	UPROPERTY(EditAnywhere)
	float VignetteIntensityRateMultiplier = 0.3f;

	UPROPERTY()
	float CameraVignetteAmount;

	bool bAreDistanced = false;

	bool bIsInterpolating = false;
};
