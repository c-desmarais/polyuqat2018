// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/Character.h"
#include "BaseProjectPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class UBISOFT2018_API ABaseProjectPlayerController : public APlayerController
{
	GENERATED_BODY()

	ABaseProjectPlayerController(const FObjectInitializer& ObjectInitializer);

public:

	void BeginPlay() override;
	virtual void Tick(float deltaTime);

	FORCEINLINE UClass* GetCharacterClass() { return CharacterClass; };
	FORCEINLINE void SetCharacterClass(TSubclassOf<ACharacter> newCharacterClass) { CharacterClass = newCharacterClass; };

	UFUNCTION(Client, Reliable)
	void ClientWaitForOtherPlayer();

	UFUNCTION(Client, Reliable)
	void ClientOtherPlayerJoined();

	UFUNCTION(Client, Reliable)
	void ClientShowVictoryScreen();

	UFUNCTION(Server, WithValidation, Reliable, BlueprintCallable)
	void ServerShowMainMenu();

protected:
	virtual void SetupInputComponent();
	
	static const FName MoveForwardBinding;
	static const FName MoveSidewaysBinding;
	
	FVector GetInputDirectionVector();

	void Jump();

	void StopJumping();

	void Attack();
	void Push();

	UFUNCTION(BlueprintCallable)
	void PauseMenu();

	UPROPERTY(Category = Controls, EditAnywhere)
	float AxisDetectionThreshold = 0.2f;

	UPROPERTY(Category = UI, EditDefaultsOnly)
	TSubclassOf<class UInGameWidgetsContainer> InGameWidgetsContainerClass;

	// Returns this PC's current instance of the Widgets container
	UInGameWidgetsContainer* GetWidgetsContainer();
	

private:
	/* Pawn class used by this controller */
	UPROPERTY()
	TSubclassOf<ACharacter> CharacterClass;
	
	// This follows a single instance pattern, please use the get funtion instead
	UPROPERTY()
	UInGameWidgetsContainer* InGameWidgetsContainer;
};
