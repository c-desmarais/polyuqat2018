// Fill out your copyright notice in the Description page of Project Settings.

#include "DarklingAIController.h"
#include "DarklingCharacter.h"
#include "DepressedCharacter.h"
#include "EngineUtils.h"
#include "TimerManager.h"
#include "AI/DarklingBehaviour.h"
#include "AI/DarklingBehaviourPassive.h"
#include "Ubisoft2018GameModeBase.h"
#include "AI/HiveMind.h"
#include "Torch.h"
#include "AI/DarklingBehaviourDimTorch.h"
#include "CustomUtils.h"

void ADarklingAIController::Possess(APawn* PossessedPawn) {
	Super::Possess(PossessedPawn);

	auto darkling = Cast<ADarklingCharacter>(PossessedPawn);
	if (darkling) {
		darkling->OnStunned.AddDynamic(this, &ADarklingAIController::OnStunned);
		darkling->OnDeath.AddDynamic(this, &ADarklingAIController::OnPawnDied);
		darkling->OnDestroyed.AddDynamic(this, &ADarklingAIController::OnPawnDestroyed);
	}

	// Get reference to hivemind
	HiveMind = Cast<AUbisoft2018GameModeBase>(GetWorld()->GetAuthGameMode())->GetHiveMind();

	// Start in passive mode
	SetCurrentBehaviour(NewObject<UDarklingBehaviourPassive>());
}


float ADarklingAIController::GetDetectionRange()
{
	return EnemyDetectionRange;
}

UHiveMind * ADarklingAIController::GetHiveMind()
{
	return HiveMind;
}

void ADarklingAIController::AttackTarget(AActor * target)
{
	FVector attackVector = target->GetActorLocation() - GetDarkling()->GetActorLocation();

	GetDarkling()->Attack(attackVector);
}

void ADarklingAIController::OnPawnDied(ACharacter * darkling, bool killed)
{
	SetCurrentBehaviour(nullptr);
}

void ADarklingAIController::OnPawnDestroyed(AActor * actor)
{
}

void ADarklingAIController::OnTargetLost(ACharacter * victim, bool killed)
{
	// When the target is lost, we go back to being passive
	HiveMind->DecrementProvokedDarklings();
	SetCurrentBehaviour(NewObject<UDarklingBehaviourPassive>());
}

void ADarklingAIController::Tick(float DeltaTime)
{
	if (CurrentBehaviour) {
		CurrentBehaviour->Tick(DeltaTime);
	}
}

void ADarklingAIController::DecideNextBehaviour()
{
	if (bIsStunned) {
		return;
	}

	ATorch* nearestTorch = GetNearestProvokingTorch();
	if (nearestTorch) {
		UDarklingBehaviourDimTorch* torchBehaviour = NewObject<UDarklingBehaviourDimTorch>();
		torchBehaviour->SetTargetTorch(nearestTorch);
		SetCurrentBehaviour(torchBehaviour);
		return;
	}

	auto nearestTarget = GetClosestTarget();
	if (nearestTarget) {
		UDarklingBehaviour* selectedBehaviour = HiveMind->GetRandomBehaviour();
		selectedBehaviour->SetTarget(nearestTarget);
		SetCurrentBehaviour(selectedBehaviour);
	}
	else {
		SetCurrentBehaviour(NewObject<UDarklingBehaviourPassive>());
	}
}


ADarklingCharacter* ADarklingAIController::GetDarkling()
{
	return (ADarklingCharacter*)GetPawn();
}

void ADarklingAIController::OnMoveCompleted(FAIRequestID RequestId, const FPathFollowingResult & Result)
{
	// Nothing for now
	Super::OnMoveCompleted(RequestId, Result);

	// Advise behaviour
	CurrentBehaviour->OnMoveCompleted(Result.IsSuccess());
}

void ADarklingAIController::SetCurrentBehaviour(UDarklingBehaviour * behaviour)
{
	StopMovement();
	if (CurrentBehaviour) {
		CurrentBehaviour->OnExit();
	}
	CurrentBehaviour = behaviour;
	if (CurrentBehaviour) {
		CurrentBehaviour->SetAIController(this);
		CurrentBehaviour->OnEnter();
	}
}

void ADarklingAIController::OnStunned(float StunTime)
{
	SetCurrentBehaviour(nullptr);
	GetDarkling()->SetStatus(EDarklingStatus::SD_Stunned);
	GetWorldTimerManager().SetTimer(StunTimerHandle, this, &ADarklingAIController::EndStun, StunTime, false);
	bIsStunned = true;
}

void ADarklingAIController::EndStun()
{
	bIsStunned = false;
	DecideNextBehaviour();
}

ATorch * ADarklingAIController::GetNearestProvokingTorch()
{
	ATorch* nearestTorch = Cast<ATorch>(UCustomUtils::GetNearestActorOfType<ATorch>(GetDarkling()->GetActorLocation(), GetWorld()));
	if (nearestTorch && nearestTorch -> GetIsLit() && nearestTorch->GetProvocationCollider()->IsOverlappingActor(GetDarkling())) {
		return nearestTorch;
	}
	else {
		return nullptr;
	}
}

ABaseProjectCharacter * ADarklingAIController::GetClosestTarget()
{
	TArray<AActor*> targets;
	AActor* gloom = HiveMind->GetGloomCharacter();
	if (gloom) {
		targets.Add(gloom);
	}
	AActor* lumi = HiveMind->GetLumiCharacter();
	if (lumi) {
		targets.Add(lumi);
	}

	FVector darklingLocation = GetDarkling()->GetActorLocation();

	AActor* nearest = UCustomUtils::GetNearestActor(darklingLocation, targets);
	if (GetDetectionRange() >= FVector::Dist(nearest->GetActorLocation(), darklingLocation)){
		return Cast<ABaseProjectCharacter>(nearest);
	}
	else {
		return nullptr;
	}
}
