// Fill out your copyright notice in the Description page of Project Settings.

#include "CustomUtils.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"

inline AActor * UCustomUtils::GetNearestActor(const FVector & Location, const TArray<AActor*>& Actors)
{
	AActor* nearest = nullptr;
	float closestDistance = -1.0;
	for (AActor* actor : Actors) {
		float distance = FVector::Dist(Location, actor->GetActorLocation());
		if (distance <= closestDistance || closestDistance == -1.0) {
			closestDistance = distance;
			nearest = actor;
		}
	}
	return nearest;
}
