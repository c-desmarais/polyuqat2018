// Fill out your copyright notice in the Description page of Project Settings.

#include "Checkpoint.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Ubisoft2018GameModeBase.h"
#include "Components/BoxComponent.h"
#include "DepressedCharacter.h"
#include "LightCharacter.h"


// Sets default values
ACheckpoint::ACheckpoint()
{
	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	Box->SetupAttachment(RootComponent);
	RootComponent = Box;

	Box->OnComponentBeginOverlap.AddDynamic(this, &ACheckpoint::OnOverlapBegin);
}

AActor * ACheckpoint::GetYootaStart()
{
	return YootaStart;
}

AActor * ACheckpoint::GetLumiStart()
{
	return LumiStart;
}

// Called when the game starts or when spawned
void ACheckpoint::BeginPlay()
{
	Super::BeginPlay();
	
	if (bIsStartingCheckpoint) {
		BecomeActiveCheckpoint();
	}
}

void ACheckpoint::BecomeActiveCheckpoint()
{
	AUbisoft2018GameModeBase* gameMode = (AUbisoft2018GameModeBase*)(GetWorld()->GetAuthGameMode());
	if (gameMode != nullptr) {
		gameMode->SetCheckpoint(this);
		bAlreadyActive = true;
	}
}

void ACheckpoint::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (!bAlreadyActive) {
		ADepressedCharacter* gloom = Cast<ADepressedCharacter>(OtherActor);
		if (gloom) {
			bGloomActivated = true;
		}
		ALightCharacter* lumi = Cast<ALightCharacter>(OtherActor);
		if (lumi) {
			bLumiActivated = true;
		}
		if (bLumiActivated && bGloomActivated) {
			BecomeActiveCheckpoint();
		}
	}
}

