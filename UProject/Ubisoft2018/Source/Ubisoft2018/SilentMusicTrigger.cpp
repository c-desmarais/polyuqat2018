// Fill out your copyright notice in the Description page of Project Settings.

#include "SilentMusicTrigger.h"
#include "SoundManager.h"
#include "DepressedCharacter.h"
#include "LightCharacter.h"


// Sets default values
ASilentMusicTrigger::ASilentMusicTrigger()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASilentMusicTrigger::BeginPlay()
{
	Super::BeginPlay();

}

void ASilentMusicTrigger::ManageSilence()
{
	bIsMusicPlaying = false;
	if (FadeEffect) {
		SoundManager->FadeOutSound();
	}
	else {
		SoundManager->StopSound();
	}
}

void ASilentMusicTrigger::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (bActivated || !SoundManager || !(OtherActor->IsA(ADepressedCharacter::StaticClass()) || OtherActor->IsA(ALightCharacter::StaticClass()))) {
		return;
	}

	if (OtherActor->IsA(ADepressedCharacter::StaticClass()) || OtherActor->IsA(ALightCharacter::StaticClass())) {
		bActivated = true;
		SoundManager->SetLastActivatedTrigger(this);
		ManageSilence();
	}
}