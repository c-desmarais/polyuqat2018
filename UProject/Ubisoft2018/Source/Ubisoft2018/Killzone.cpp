// Fill out your copyright notice in the Description page of Project Settings.

#include "Killzone.h"
#include "BaseProjectCharacter.h"
#include "Components/BoxComponent.h"


// Sets default values
AKillzone::AKillzone()
{
	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	Box->SetupAttachment(RootComponent);
	RootComponent = Box;

	Box->OnComponentBeginOverlap.AddDynamic(this, &AKillzone::OnOverlapBegin);
}

void AKillzone::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	// Check if it is a character
	ABaseProjectCharacter* character = Cast<ABaseProjectCharacter>(OtherActor);
	if (character) {
		// If so, kill the character
		character->Die();
	}
}

