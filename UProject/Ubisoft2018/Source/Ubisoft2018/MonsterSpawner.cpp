// Fill out your copyright notice in the Description page of Project Settings.

#include "MonsterSpawner.h"
#include "Activator.h"
#include "Runtime/Engine/Classes/Components/SpotLightComponent.h"
#include "Engine/World.h"
#include "DarklingCharacter.h"
#include "TimerManager.h"
#include "Bridge.h"
#include "WorldChangerVolume.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "EngineUtils.h"
#include "DarklingAIController.h"
#include "AI/DarklingBehaviourAggressive.h"
#include "CustomUtils.h"
#include "DepressedCharacter.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "LightCharacter.h"
#include "Torch.h"
#include "SoundManager.h"

// Sets default values
AMonsterSpawner::AMonsterSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);

	SpotLightComponent = CreateDefaultSubobject<USpotLightComponent>(TEXT("SpotLight"));
	SpotLightComponent->SetupAttachment(RootComponent);

	ShieldEffectsRoot = CreateDefaultSubobject<USceneComponent>(TEXT("ShieldFXRoot"));
	ShieldEffectsRoot->SetupAttachment(RootComponent);

	HealEffectsRoot = CreateDefaultSubobject<USceneComponent>(TEXT("HealFXRoot"));
	HealEffectsRoot->SetupAttachment(RootComponent);

	//Audio components
	SoundEffects = CreateDefaultSubobject<USceneComponent>(TEXT("SoundEffects"));
	SoundEffects->SetupAttachment(RootComponent);

	ShieldDownAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("ShieldDownAudioComponent"));
	ShieldDownAudioComponent->SetupAttachment(SoundEffects);

	ShieldUpAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("ShieldUpAudioComponent"));
	ShieldUpAudioComponent->SetupAttachment(SoundEffects);

	HitWhileImmuneAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("HitWhileImmuneAudioComponent"));
	HitWhileImmuneAudioComponent->SetupAttachment(SoundEffects);

	HitWhileVulnerableAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("HitWhileVulnerableAudioComponent"));
	HitWhileVulnerableAudioComponent->SetupAttachment(SoundEffects);

	bReplicates = true;
}

// Called when the game starts or when spawned
void AMonsterSpawner::BeginPlay()
{
	Super::BeginPlay();
	ListenToDisablers();
	SpotLightComponent->SetVisibility(false);

	if (HasAuthority()) {
		// Start spawn timer
		GetWorldTimerManager().SetTimer(SpawnTimerHandle, this, &AMonsterSpawner::PerformSpawnBurst, TimeBetweenSpawnBursts, true);
		HealthChunksLeft = HealthChunks;
		Health = CurrentMaxHealth = MaxHealth;
	}

	OnDestroyed.AddDynamic(this, &AMonsterSpawner::OnDestroyedMonsterSpawner);

	CrystalMat = Mesh->CreateDynamicMaterialInstance(0);

	for (TActorIterator<ASoundManager> itr(GetWorld()); itr; ++itr) {
		SoundManager = *itr;
	}

	NbMonsterSpawnerNeighbours = MonsterSpawnerNeighbours.Num();
}

void AMonsterSpawner::ListenToDisablers()
{
	for (AActivator * disabler : InvulnerabilityDisablers) {
		disabler->OnActivate.AddDynamic(this, &AMonsterSpawner::DisableInvulnerability);
		disabler->OnDeactivate.AddDynamic(this, &AMonsterSpawner::StartVulnerabilityCountdown);
	}
}


void AMonsterSpawner::DisableInvulnerability()
{
	SpotLightComponent->SetVisibility(true);
	SetLightAngle(MaxLightAngle);

	if (bIsInvulnerable) {
		PlayShieldSoundEffects(false);
	}

	ShieldEffectsRoot->SetHiddenInGame(true, true);
	bIsInvulnerable = false;
	VulnerabilityTimeLeft = VulnerabilityDuration;
	bCountingDownVulnerability = false;
	if (HasAuthority()) {
		AlertNearbyDarklings();
	}
}

void AMonsterSpawner::EnableInvulnerability()
{
	if (bIsInvulnerable) {
		return;
	}
	PlayShieldSoundEffects(true);
	SpotLightComponent->SetVisibility(false);
	ShieldEffectsRoot->SetHiddenInGame(false, true);
	bIsInvulnerable = true;
	bCountingDownVulnerability = false;
}

void AMonsterSpawner::StartVulnerabilityCountdown()
{
	bCountingDownVulnerability = true;
}

void AMonsterSpawner::PerformSpawnBurst()
{
	int nbOfMonstersToSpawn = FMath::Min(MonstersSpawnedPerBurst, MaxMonstersAtATime - MonsterCount);
	for (int i = 0; i < nbOfMonstersToSpawn; ++i) {
		ADarklingCharacter* monster = SpawnMonster();
		if (monster) {
			MonsterCount++;
			monster->OnDeath.AddDynamic(this, &AMonsterSpawner::OnMonsterDeath);
		}
	}
}

void AMonsterSpawner::OnMonsterDeath(ACharacter* Victim, bool bKilled)
{
	MonsterCount--;
}

AActor * AMonsterSpawner::GetNextSpawnPoint()
{
	if (MonsterSpawnPoints.Num() == 0) {
		UE_LOG(LogTemp, Warning, TEXT("Monster spawner has no assigned spawn points"));
		return nullptr;
	}

	AActor* spawnPoint = MonsterSpawnPoints[MonsterSpawnpointIndex];
	// Update index for next spawn point acquisition
	MonsterSpawnpointIndex = (MonsterSpawnpointIndex + 1) % MonsterSpawnPoints.Num();
	return spawnPoint;
}

ADarklingCharacter* AMonsterSpawner::SpawnMonster()
{
	ADarklingCharacter* monster = nullptr;
	AActor* spawnPoint = GetNextSpawnPoint();

	if (!spawnPoint) {
		return nullptr;
	}

	// Keep trying to spawn until all options have run out
	int attempts = 0;
	while (!monster && attempts < MonsterSpawnPoints.Num()) {
		FActorSpawnParameters spawnParams;
		spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		monster = GetWorld()->SpawnActor<ADarklingCharacter>(MonsterClass, GetNextSpawnPoint()->GetActorTransform(), spawnParams);
		attempts++;
	}
	return monster;
}

void AMonsterSpawner::MulticastDamageEffect_Implementation(bool IsInvulnerable)
{
	if (IsInvulnerable) {
		// TODO: Play SFX and VFX for invulnerable
		if (HitWhileImmuneAudioComponent) {
			HitWhileImmuneAudioComponent->Play();
		}
	}
	else{
		if (DamageParticleEffects) {
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DamageParticleEffects, GetActorLocation() + FVector(0, 0, 100));
		}

		if (HitWhileVulnerableAudioComponent) {
			HitWhileVulnerableAudioComponent->Play();
		}
	}
}

void AMonsterSpawner::HandleDestruction()
{
	GetWorldTimerManager().ClearTimer(SpawnTimerHandle); 

	MulticastDestructionEffect();

	Destroy();
}


void AMonsterSpawner::MulticastDestructionEffect_Implementation()
{
	Activate();
}

// Called every frame
void AMonsterSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bCountingDownVulnerability) {
		ComputeVulnerabilityTick(DeltaTime);
	}

	if (HasAuthority()) {
		bIsHealing = bIsInvulnerable && Health < CurrentMaxHealth;
		if (bIsHealing) {
			Health = FMath::Min(Health + HealthRegenRate*DeltaTime, CurrentMaxHealth);
		}
	}
	if (bIsHealing != HealEffectsRoot->IsVisible()) {
		HealEffectsRoot->SetVisibility(bIsHealing, true);
	}
}

void AMonsterSpawner::ComputeVulnerabilityTick(float deltaTime)
{
	float lightAngle = MinLightAngle + (MaxLightAngle-MinLightAngle)*(VulnerabilityTimeLeft / VulnerabilityDuration);
	SetLightAngle(lightAngle);
	VulnerabilityTimeLeft -= deltaTime;
	if (VulnerabilityTimeLeft <= 0) {
		EnableInvulnerability();
	}
}

float AMonsterSpawner::TakeDamage(float DamageAmount, FDamageEvent const & DamageEvent, AController * EventInstigator, AActor * DamageCauser)
{

	if (!HasAuthority()) {
		return 0.0f;
	}

	MulticastDamageEffect(bIsInvulnerable);

	if (!bIsInvulnerable) {

		// Call the base class - this will tell us how much damage to apply  
		const float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
		if (ActualDamage > 0.f)
		{
			Health -= ActualDamage;

			// If the damage depletes our health, kill the actor  
			if (Health <= 0.f)
			{
				HandleDestruction();
			}
			else if (Health <= MaxHealth*(HealthChunksLeft - 1)/HealthChunks) {
				HandleChunkLoss();
			}
		}

		return ActualDamage;
	}

	return 0.0f;
}

void AMonsterSpawner::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AMonsterSpawner, Health);
	DOREPLIFETIME(AMonsterSpawner, bIsHealing);
}

float AMonsterSpawner::GetHealth()
{
	return Health;
}

int AMonsterSpawner::GetHealthChunks()
{
	return HealthChunks;
}

bool AMonsterSpawner::GetIsInvulnerable()
{
	return bIsInvulnerable;
}

void AMonsterSpawner::AlertNearbyDarklings()
{
	ABaseProjectCharacter* target = GetActivatingPlayer();
	if (!target) {
		return;
	}

	for (TActorIterator<ADarklingCharacter> itr(GetWorld()); itr; ++itr) {
		if (FVector::Dist(itr->GetActorLocation(), GetActorLocation()) <= AlertRadius) {
			auto behaviour = NewObject<UDarklingBehaviourAggressive>();
			behaviour->SetTarget(target);
			behaviour->SetDuration(10.0f);
			auto AIController = Cast<ADarklingAIController>(itr->GetController());
			AIController->SetCurrentBehaviour(behaviour);
		}
	}
}

ABaseProjectCharacter * AMonsterSpawner::GetActivatingPlayer()
{
	for (auto disabler : InvulnerabilityDisablers) {
		auto character = UCustomUtils::GetNearestActorOfType<ABaseProjectCharacter>(disabler->GetActorLocation(), GetWorld());
		if (character && (character->IsA(ADepressedCharacter::StaticClass()) || character->IsA(ALightCharacter::StaticClass()))) {
			return character;
		}
	}
	return nullptr;
}

void AMonsterSpawner::HandleChunkLoss()
{
	HealthChunksLeft--;
	CurrentMaxHealth = MaxHealth*HealthChunksLeft / HealthChunks;
	MulticastChunkLossEffect(CurrentMaxHealth);
}

void AMonsterSpawner::MulticastChunkLossEffect_Implementation(float newMaxHealth)
{
	CrystalMat->SetScalarParameterValue(FName("NearDeath"), 1-(newMaxHealth / MaxHealth));
}

void AMonsterSpawner::SetLightAngle(float Angle)
{
	SpotLightComponent->SetInnerConeAngle(0.85*Angle);
	SpotLightComponent->SetOuterConeAngle(Angle);
}

void AMonsterSpawner::OnDestroyedMonsterSpawner(AActor * Act)
{
	//Spawn damage particles
	if (DestroyParticleEffects) {
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DestroyParticleEffects, GetActorLocation());
	}

	//Play sound
	if(DestroySound){
		UGameplayStatics::PlaySound2D(GetWorld(), DestroySound);
	}
	
	MulticastLightTorches();

	if (DestructionForceFeedback)
	{
		UGameplayStatics::SpawnForceFeedbackAtLocation(GetWorld(), DestructionForceFeedback, GetActorLocation());
	}

	//Destroy All Darklings in vicinity
	for (ATorch* Torch : Torches) {
		if (Torch) {
			TArray<AWorldChangerVolume*> worldChangers = Torch->GetWorldChangers();
			for (AWorldChangerVolume* worldChanger : worldChangers) {
				if (worldChanger) {
					worldChanger->DestroyAllDarklingsInArea();
				}
			}
		}
	}

	for (AMonsterSpawner* ms : MonsterSpawnerNeighbours) { // in case of 2 monster spawners
		if (ms) ms->DecrementNbMonsterSpawnerNeighbours();
	}
	
	if (NbMonsterSpawnerNeighbours == 0) { 
		SoundManager->FadeInSound(SoundMusicAfterDestroy, MusicFadeTimeAfterDestroy); // Play music 
	}

}

void AMonsterSpawner::DecrementNbMonsterSpawnerNeighbours() {
	NbMonsterSpawnerNeighbours--;
}

void AMonsterSpawner::PlayShieldSoundEffects(bool isEnabled)
{
	if (isEnabled) {
		if (ShieldUpAudioComponent) {
			ShieldUpAudioComponent->Play();
		}
	}
	else {
		if (ShieldDownAudioComponent) {
			ShieldDownAudioComponent->Play();
		}
	}
}

void AMonsterSpawner::MulticastLightTorches_Implementation()
{
	for (ATorch* Torch : Torches) {
		if (Torch) {
			Torch->LightTorch();
		}
	}
}
