// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Checkpoint.generated.h"

class UBoxComponent;

UCLASS()
class UBISOFT2018_API ACheckpoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACheckpoint();

	AActor* GetYootaStart();

	AActor* GetLumiStart();

	// Mark this checkpoint as the beginning of the level
	UPROPERTY(EditAnywhere)
	bool bIsStartingCheckpoint = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Sets this checkpoint as the active checkpoint
	void BecomeActiveCheckpoint();

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UBoxComponent* Box;

	UPROPERTY(EditAnywhere)
	AActor* YootaStart;

	UPROPERTY(EditAnywhere)
	AActor* LumiStart;

	UPROPERTY()
		bool bLumiActivated = false;

	UPROPERTY()
		bool bGloomActivated = false;

	UPROPERTY()
		bool bAlreadyActive = false;
};
