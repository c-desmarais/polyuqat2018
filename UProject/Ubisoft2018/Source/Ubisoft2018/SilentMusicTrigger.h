// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseMusicTrigger.h"
#include "SilentMusicTrigger.generated.h"

class ALightCharacter;
class ADepressedCharacter;

UCLASS()
class UBISOFT2018_API ASilentMusicTrigger : public ABaseMusicTrigger
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASilentMusicTrigger();

protected:
	// Functions
	virtual void BeginPlay() override;

	void ManageSilence();

	UFUNCTION()
		virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	int CounterLightOverlapped = 0;

	int CounterDepressedOverlapped = 0;

	bool bIsMusicPlaying = false;

	ALightCharacter* lightCharacter;
	ADepressedCharacter* depressedCharacter;

	bool bActivated = false;
};
