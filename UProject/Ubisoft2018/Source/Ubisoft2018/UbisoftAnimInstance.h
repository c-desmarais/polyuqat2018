// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "UbisoftAnimInstance.generated.h"

/** Delegate for notification */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNotificationSignature);

USTRUCT(BlueprintType)
struct FAnimParameters {
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool bIsFalling = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool bIsDying = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool bIsTakingDamage = false;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool bIsAttacking = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool bIsJumping = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool bIsAggro = false;
};

/**
 * 
 */
UCLASS()
class UBISOFT2018_API UUbisoftAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:

	void SetAnimationParameters(const FAnimParameters & newParams);

	void SetMovementSpeed(float Speed);
	
	UPROPERTY(BlueprintReadOnly)
	FAnimParameters AnimationParameters;

	UPROPERTY(BlueprintCallable)
	FNotificationSignature OnResolveAttack;

	UPROPERTY(BlueprintCallable)
	FNotificationSignature OnAttackCompleted;

	UPROPERTY(BlueprintCallable)
	FNotificationSignature OnHurtCompleted;

	UPROPERTY(BlueprintCallable)
	FNotificationSignature OnDeathAnimCompleted;

	UPROPERTY(BlueprintCallable)
	FNotificationSignature OnJumpAnimCompleted;

	UPROPERTY(BlueprintCallable)
	FNotificationSignature OnAggroAnimCompleted;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MovementSpeed = 0.0f;

};
