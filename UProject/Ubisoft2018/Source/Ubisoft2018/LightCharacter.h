// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseProjectCharacter.h"
#include "LightCharacter.generated.h"

class USphereComponent;
class ATorch;

UCLASS()
class UBISOFT2018_API ALightCharacter : public ABaseProjectCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ALightCharacter();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;

	UFUNCTION(Server, Reliable, WithValidation)
	virtual void ServerDispatchLightAction() override;

	void SetNearbyTorch(ATorch* torch);

protected:

	virtual void SetupAnimInstance() override;

	virtual void Tick(float DeltaTime) override;

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerJump();

	// Lumi jumps on all clients
	UFUNCTION(NetMulticast, Reliable)
	void MulticastJump();

	UFUNCTION()
	void OnJumpCompleted();

	virtual bool CanAttack() override;

	UFUNCTION(Server, Reliable, WithValidation)
	virtual void ServerEnqueueAttackRequest(FVector AttackDirection) override;

	// Everyone creates the projectile
	UFUNCTION(NetMulticast, Reliable)
	void MulticastSpawnWaveProjectile(FVector Location, FVector LaunchDirection);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastEnableAttack();

	void EnableAttack();
	
	UFUNCTION(NetMulticast, Reliable)
	void MulticastSetAimDecalVisibility(bool visible);

	void SetAimDecalVisibility(bool visible);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerLightTorch(ATorch* Torch);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastLightTorch(ATorch* Torch);

	UPROPERTY(EditAnywhere)
	USceneComponent * AimVisualsRoot;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AWaveProjectile> ProjectileClass;

	UPROPERTY(EditAnywhere)
	float AttackCoolDown = 1.0f;
	
	UPROPERTY()
	bool bAttackOnCooldown = false;
	
	FTimerHandle AttackCoolDownTimerHandle;

	ATorch* NearbyTorch = nullptr;

};
