// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "UserWidget.h"
#include "InGameWidgetsContainer.generated.h"


UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EWidgetEnum : uint8
{
	WE_Pause 		UMETA(DisplayName = "PauseMenu"),
	WE_LevelEnd 	UMETA(DisplayName = "LevelEnd"),
	WE_Waiting		UMETA(DisplayName = "WaitingForPlayer"),
	WE_Joined		UMETA(DisplayName = "PlayerJoined"),
	WE_HUD			UMETA(DisplayName = "HUD"),
	WE_Credits		UMETA(DisplayName = "Credits"),
	WE_MainMenu		UMETA(DisplayName = "MainMenu")
};

UCLASS(Blueprintable)
class UBISOFT2018_API UInGameWidgetsContainer : public UObject
{
	GENERATED_BODY()

public:
	void SetParentController(class APlayerController* controller);

	UFUNCTION(BlueprintCallable)
	UUserWidget* OpenWidget(EWidgetEnum widgetType);

	UFUNCTION(BlueprintCallable)
	void CloseWidget(EWidgetEnum widgetType);

	bool IsWidgetOpen(EWidgetEnum widgetType);

protected:

	UPROPERTY()
	TMap<EWidgetEnum, UUserWidget*> OpenWidgets;

	UPROPERTY(EditDefaultsOnly)
	TMap<EWidgetEnum, TSubclassOf<UUserWidget>> WidgetsClasses;

	APlayerController* PlayerController;
};
