// Fill out your copyright notice in the Description page of Project Settings.

#include "Ubisoft2018GameModeBase.h"
#include "BaseProjectPlayerController.h"
#include "EngineUtils.h"
#include "LevelCamera.h"
#include "Checkpoint.h"
#include "BaseProjectCharacter.h"
#include "AI/HiveMind.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

AUbisoft2018GameModeBase::AUbisoft2018GameModeBase() {
	PlayerControllerClass = ABaseProjectPlayerController::StaticClass();
}

void AUbisoft2018GameModeBase::HandleStartingNewPlayer_Implementation(APlayerController * newPlayer)
{
	ABaseProjectPlayerController* playerController = Cast<ABaseProjectPlayerController>(newPlayer);

	if (GetNumPlayers() < 2) {
		// assign the depressed character class
		playerController->SetCharacterClass(DepressedCharacterClass);
		playerController->ClientWaitForOtherPlayer();
	}
	else {
		// assign light character class
		playerController->SetCharacterClass(LightCharacterClass);
		SpawnAllPlayers();
	}
}

void AUbisoft2018GameModeBase::Logout(AController * controller)
{
	if (!controller->IsLocalPlayerController()) {
		UGameplayStatics::OpenLevel(GetGameInstance(), FName(TEXT("MainMenu")));
	}
}

UClass * AUbisoft2018GameModeBase::GetDefaultPawnClassForController_Implementation(AController * InController)
{
	ABaseProjectPlayerController* playerController = Cast<ABaseProjectPlayerController>(InController);
	return playerController->GetCharacterClass();
}

void AUbisoft2018GameModeBase::RestartPlayer(AController* player) 
{
	Super::RestartPlayer(player);

	ABaseProjectCharacter* playerCharacter = (ABaseProjectCharacter*)(player->GetPawn());

	if (playerCharacter) {
		playerCharacter->OnDeath.AddDynamic(this, &AUbisoft2018GameModeBase::HandleDeath);
		
		// Notify hivemind of player spawning
		if (playerCharacter->IsA(DepressedCharacterClass)) {
			GetHiveMind()->SetGloomCharacter(playerCharacter);
		}
		else {
			GetHiveMind()->SetLumiCharacter(playerCharacter);
		}
	}
}

AActor * AUbisoft2018GameModeBase::FindPlayerStart_Implementation(AController * Player, const FString & IncomingName)
{
	if (CurrentCheckpoint == nullptr) {
		SetCheckpoint(FindStartingCheckpoint());
		
		// If still no checkpoint, use default implementation
		if (CurrentCheckpoint == nullptr) {
			UE_LOG(LogTemp, Error, TEXT("No checkpoints found in game mode, players spawning the default way"))
			return Super::FindPlayerStart_Implementation(Player, IncomingName);
		}
	}

	// Spawn player at the appropriate start point
	ABaseProjectPlayerController* playerController = Cast<ABaseProjectPlayerController>(Player);
	if (playerController->GetCharacterClass() == DepressedCharacterClass) {
		return CurrentCheckpoint->GetYootaStart();
	}
	else {
		return CurrentCheckpoint->GetLumiStart();
	}
}

APawn * AUbisoft2018GameModeBase::SpawnDefaultPawnAtTransform_Implementation(AController * NewPlayer, const FTransform & SpawnTransform)
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.Instigator = Instigator;
	SpawnInfo.ObjectFlags |= RF_Transient;	// We never want to save default player pawns into a map
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	UClass* PawnClass = GetDefaultPawnClassForController(NewPlayer);
	APawn* ResultPawn = GetWorld()->SpawnActor<APawn>(PawnClass, SpawnTransform, SpawnInfo);
	if (!ResultPawn)
	{
		UE_LOG(LogGameMode, Warning, TEXT("SpawnDefaultPawnAtTransform: Couldn't spawn Pawn of type %s at %s"), *GetNameSafe(PawnClass), *SpawnTransform.ToHumanReadableString());
	}
	return ResultPawn;
}

void AUbisoft2018GameModeBase::SetCheckpoint(ACheckpoint * newCheckpoint)
{
	CurrentCheckpoint = newCheckpoint;
}

void AUbisoft2018GameModeBase::HandleDeath(ACharacter* DeadCharacter, bool bKilled)
{
	AController* controller = DeadCharacter->GetController();

	DeadCharacter->Destroy();
	// Notify hivemind of player death

	// Notify hivemind of player spawning
	if (DeadCharacter->IsA(DepressedCharacterClass)) {
		DarklingHiveMind->OnGloomDied();
	}
	else {
		DarklingHiveMind->OnLumiDied();
	}

	RestartPlayer(controller);
}

void AUbisoft2018GameModeBase::EndGame()
{
	auto PCIter = GetWorld()->GetPlayerControllerIterator();
	while (PCIter) {
		ABaseProjectPlayerController* playerController = Cast<ABaseProjectPlayerController>(PCIter->Get());
		if (playerController) {
			playerController->ClientShowVictoryScreen();
		}
		++PCIter;
	}
}

UHiveMind * AUbisoft2018GameModeBase::GetHiveMind()
{
	if (!DarklingHiveMind) {
		DarklingHiveMind = NewObject<UHiveMind>(this, DarklingHiveMindClass);
	}
	return DarklingHiveMind;
}

ACheckpoint* AUbisoft2018GameModeBase::FindStartingCheckpoint()
{
	for (TActorIterator<ACheckpoint> checkpointIterator(GetWorld()); checkpointIterator; ++checkpointIterator) {
		if (checkpointIterator->bIsStartingCheckpoint) {
			return *checkpointIterator;
		}
	}
	return nullptr;
}

void AUbisoft2018GameModeBase::SpawnAllPlayers()
{
	auto PCIter = GetWorld()->GetPlayerControllerIterator();
	while (PCIter) {
		ABaseProjectPlayerController* playerController = Cast<ABaseProjectPlayerController>(PCIter->Get());
		if (playerController) {
			playerController->ClientOtherPlayerJoined();
			RestartPlayer(playerController);
		}
		++PCIter;
	}
}


