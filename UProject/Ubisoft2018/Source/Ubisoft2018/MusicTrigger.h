// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseMusicTrigger.h"
#include "MusicTrigger.generated.h"

class ASoundManager;
class ABaseProjectCharacter;

UCLASS()
class UBISOFT2018_API AMusicTrigger : public ABaseMusicTrigger
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMusicTrigger();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void ManageBothSounds();

	void ManageOneSound();

	UFUNCTION()
		virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	UPROPERTY(EditAnywhere)
		USoundBase* Sound;

	UPROPERTY(EditAnywhere)
		USoundBase* OptionalLoopingSound = nullptr;

	bool SoundIsPlaying = false;
	
	bool HasBeenOverlapped = false;
	
	
};
