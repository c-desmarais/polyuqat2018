// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Volume.h"
#include "WorldChangerVolume.generated.h"

class UColorShiftingComponent;
class ASoundManager;

/**
 * 
 */
UCLASS()
class UBISOFT2018_API AWorldChangerVolume : public AVolume
{
	GENERATED_BODY()
	
	AWorldChangerVolume();

public:
	UFUNCTION()
	void ChangeColorMode(bool isInLightMode);

	UFUNCTION()
	void ChangeWorld(bool isInLightMode);

	UFUNCTION()
	void DestroyAllDarklingsInArea();

	void AddLitTorch();

	void RemoveLitTorch();

	void AffectCharacters();

	bool IsInLightMode();

protected:

	void BeginPlay() override;

	void SetupWorldChanger();

	void AffectDepressedCharacter(class ADepressedCharacter* character, bool isCharacterAffected);
	
	void AffectDarklingCharacter(class ADarklingCharacter* character, bool isCharacterAffected);

	UFUNCTION()
	void OnOverlapBegin(AActor* MyOverlappedActor, AActor* OtherActor);
	
	UFUNCTION()
	void OnOverlapEnd(AActor* MyOverlappedActor, AActor* OtherActor);

	bool IsChangeable(AActor* Actor);

	UPROPERTY(EditAnywhere)
	UColorShiftingComponent* ColorShift;

	UPROPERTY(EditAnywhere)
	float DarklingSpeedRatio = 0.4f;

	UPROPERTY(EditAnywhere)
	float GloomSpeedRatio = 1.7f;

	bool bIsInLightMode = false;

	TArray<class ABaseProjectCharacter*> AffectedCharacters;

	int NbOfLitTorches = 0;

	bool bIsLitByMultipleTorches = false;
};
